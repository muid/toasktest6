import { Router, RouterContext } from "../../deps/oak.ts";
//import { MyContext } from "../common/MyContext.ts";
import { UserController } from "../../common/users/UserController.ts";
import { renderFile } from "../../deps/pug.ts";

export function use(path: string, router: Router, userController: UserController) {
	router.get(`${path}`, async (ctx: RouterContext) => {
		if (!ctx.request.hasBody) {
			//return BadRequest(ctx);
		}

		const value = ctx.params;

		var availableApps: string[] = [];

		if (value != null && value.user != null) {
			var userName = value.user;
			availableApps.push("todo");
			availableApps.push("tasks");
			availableApps.push("coins");
			availableApps.push("journal");
			availableApps.push("kb");
			availableApps.push("unsorted");
		}

		ctx.response.body = renderFile("common/views/apps.pug", { username: value.user, apps: availableApps });
	});
}
