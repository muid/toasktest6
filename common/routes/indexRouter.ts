import { Router, Context } from "../../deps/oak.ts";
//import { MyContext } from "../common/MyContext.ts";
import { UserController } from "../../common/users/UserController.ts";
import { renderFile } from "../../deps/pug.ts";

export function use(path: string, router: Router, userController: UserController) {
	router.get(`${path}`, async (ctx: Context) => {
		userController.userGetNames((userNames: []) => {
			ctx.response.body = renderFile("common/views/index.pug", { userNames: userNames });
		});
	});
}
