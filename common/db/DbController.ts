import { v4 } from "../../deps/tools.ts";
import { connect } from "../../deps/db.ts";
import { MyContext } from "../MyContext.ts";

export enum SQLTYPE {
	NUMBER = 1,
	BOOL,
	TEXT,
	UUID,
}

type DatabaseValues = string | number | Date | boolean | null | undefined;

interface DatabaseResult {
	[key: string]: DatabaseValues;
}

type Adapter = {
	query(query: string, values?: [DatabaseValues]): Promise<DatabaseResult[]>;
	connect(): Promise<void>;
	disconnect(): Promise<void>;
};

export class DbController {
	db: any;
	myContext: MyContext;
	dbPath: string;

	constructor(path: string, myContext: MyContext) {
		this.dbPath = path;
		this.myContext = myContext;
		console.log(path);
		this.db = null;
	}

	async query(query: string) {
		if (!this.db) {
			this.db = await connect({
				type: "sqlite",
				database: this.dbPath,
			});
		}

		var ret: any;
		try {
			console.log(query);
			ret = await this.db.query(query);
		} catch (ex) {
			console.log(ex + query);
		}
		return ret;
	}
	async getDb() {
		return await this.db;
	}

	getSQLValueBase(column: string, type: number, tableName: string | undefined) {
		var ret = column;
		var prefix = "";

		if (tableName) {
			prefix = `${tableName}.`;
		}

		if (column) {
			if (type == SQLTYPE.NUMBER) {
				ret = `${prefix}${column}`;
			} else if (type == SQLTYPE.UUID) {
				ret = `(CASE WHEN ${prefix}${column} IS NOT NULL THEN SUBSTR(QUOTE(${prefix}${column}), 3, 32) END)`;
			}
		}

		return ret;
	}

	getSQLValue(column: string, type: number, tableName?: string) {
		var ret = this.getSQLValueBase(column, type, tableName);
		var asColumn = "";
		var prefix = "";

		if (tableName) {
			prefix = `${tableName}.`;
		} else {
			asColumn = ` AS ${column}`;
		}

		ret += ` ${asColumn}`;

		return ret;
	}

	getSQLValueShort(column: string, type: number, tableName: string | undefined) {
		var ret = this.getSQLValueBase(column, type, tableName);

		if (type == SQLTYPE.UUID) {
			ret += ` AS ${column}`;
		}

		return ret;
	}

	getSQLLikeTerm(field: any, termArr: any[], isNot: any) {
		var ret = "";
		var first = true;
		var notLike = "";
		if (isNot) {
			notLike = " NOT ";
		}

		if (termArr) {
			for (var i = 0; i < termArr.length; i++) {
				var t = termArr[i];
				if (t) {
					if (!first) {
						ret += " AND ";
					}
					ret += `${field} ${notLike} like "%${t}%"`;
					first = false;
				}
			}
		}

		return ret;
	}

	setSQLValue(value: string | number | boolean | undefined, type: SQLTYPE) {
		var ret = value;

		try {
			if (value == undefined) {
				if (type == SQLTYPE.NUMBER) {
					ret = undefined;
				} else if (type == SQLTYPE.TEXT) {
					ret = "";
				} else if (type == SQLTYPE.BOOL) {
					ret = false;
				} else if (type == SQLTYPE.UUID) {
					//if (typeof value == 'string')
					{
						var uuidStr = v4.generate(); // uuid.v4();
						uuidStr = uuidStr.replace(/-/g, "");
						uuidStr = `X'${uuidStr}'`;
						ret = uuidStr;
					}
				}
			} else {
				if (value) {
					if (type == SQLTYPE.NUMBER) {
						if (value == "null") {
							ret = 0;
						} else if (value == -1) {
							ret = undefined;
						}
					} else if (type == SQLTYPE.BOOL) {
						if (value == "null") {
							ret = false;
						} else {
							ret = true;
						}
					} else if (type == SQLTYPE.UUID) {
						if (value == "") {
							ret = "''";
						} else {
							uuidStr = (value as string).replace(/-/g, "");
							uuidStr = `X'${uuidStr}'`;
							ret = uuidStr;
						}
					}
				} else {
					if (type == SQLTYPE.NUMBER) {
						ret = 0;
					} else if (type == SQLTYPE.TEXT) {
						ret = "";
					} else if (type == SQLTYPE.BOOL) {
						ret = false;
					}
				}
			}
		} catch (error) {
			var i = 2;
		}
		return ret;
	}

	uuidCreate() {
		return v4.generate();
	}

	uuidCreateEmpty() {
		var ret = "";
		ret = "0".repeat(32);
		return ret;
	}

	uuidToShortUuid(uuidStr: string) {
		var ret = "";

		if (uuidStr) {
			ret = uuidStr.replace(/-/g, "");
		}

		return ret;
	}

	uuidToStr() {}

	async getMaxIdInt(tableName: any, type: any, cb: { (maxId: any): void; (arg0: number): void }) {
		//var d = await this.db;
		var sql = `SELECT MAX(idint) as maxid FROM ${tableName} `;
		sql += ` WHERE type = ${type}`;

		//var rows = await (await this.db).query(sql);
		var rows = await this.query(sql);
		if (cb) {
			if (rows[0]) {
				cb(rows[0].maxid);
			} else {
				cb(-1);
			}
		}
	}

	async updateId(tableName: any, existingIdColName: any, existingIdValue: number, newIdColName: string, newIdValue: any, cb: { (): void; (): void }) {
		if (existingIdValue != -1) {
			if (!newIdColName) {
				newIdColName = "uid";
			}

			var uuidStr = v4.generate();
			if (newIdValue) {
				uuidStr = newIdValue;
			}
			//uuidStr = uuidStr.replace(/-/g,'');
			//uuidStr =`X'${uuidStr}'`;
			var sql = `UPDATE ${tableName} `;
			//sql += `SET gid = ${uuidStr}`;
			sql += `SET ${newIdColName} = ${this.setSQLValue(uuidStr, SQLTYPE.UUID)} `;
			sql += ` WHERE ${existingIdColName} = '${existingIdValue}'`;

			await this.query(sql);

			if (cb) {
				cb();
			}
		}
	}

	updateIdP(tableName: any, existingIdColName: any, existingIdValue: any, newIdColName: any, newIdValue?: undefined): Promise<void> {
		return new Promise((resolve, reject) => {
			this.updateId(tableName, existingIdColName, existingIdValue, newIdColName, newIdValue, () => {
				resolve();
			});
		});
	}

	async updateIdForTable(tableName: any, existingIdColName: string, newIdColName: any) {
		var self = this;
		var sql = `SELECT ${this.getSQLValue(existingIdColName, SQLTYPE.NUMBER)} FROM ${tableName} `;

		var rows = await this.query(sql);

		async function asyncRunner() {
			try {
				for (var i = 0; i < rows.length; i++) {
					//if (!rows[i].uid)
					await self.updateIdP(tableName, existingIdColName, rows[i][existingIdColName], newIdColName);
					console.log("updateIdForTable:" + i + ", " + rows[i][existingIdColName]);
				}
			} catch (error) {
				console.error(error);
			}
		}

		asyncRunner();
	}

	async insertIdAndName(tableName: any, name: any, cb?: () => void) {
		var sql = `INSERT INTO ${tableName} `;
		sql += `(id, name) VALUES (${this.setSQLValue(undefined, SQLTYPE.UUID)}, '${name}')`;

		var rows = await this.query(sql);

		if (cb) {
			cb();
		}
	}

	async updateIdInt(tableName: any, id: number, newIdValue: any, cb?: { (): void; (): void }) {
		if (id != -1) {
			var newIdColName = "idint";

			var sql = `UPDATE ${tableName} `;
			//sql += `SET gid = ${uuidStr}`;
			sql += `SET ${newIdColName} = ${this.setSQLValue(newIdValue, SQLTYPE.NUMBER)} `;
			sql += ` WHERE id = ${this.setSQLValue(id, SQLTYPE.UUID)}`;

			await this.query(sql);
			/*
				, function (err: { message: string; }) {
				if (err) {
					console.error(err.message + "\n" + sql);
				} else {
					//lastId = this.lastID;
				}

				if (cb) {
					cb();
				}
			});*/
		}
	}

	updateIdIntP(tableName: any, id: any, newIdValue: number): Promise<void> {
		return new Promise((resolve, reject) => {
			this.updateIdInt(tableName, id, newIdValue, () => {
				resolve();
			});
		});
	}

	async updateIdIntForTable(tableName: any, type: number) {
		var self = this;
		this.getMaxIdInt(tableName, type, async (maxId: number) => {
			if (maxId == null) {
				maxId = 0;
			} else if (maxId == -1) {
				return;
			}

			var sql = `SELECT ${this.getSQLValue("id", SQLTYPE.UUID)}, ${this.getSQLValue("idint", SQLTYPE.NUMBER)} FROM ${tableName} `;
			if (type != -1) {
				sql += ` WHERE type = ${type} AND idint IS NULL`;
			}
			sql += ` ORDER BY datecreated ASC`;

			var rows = await this.query(sql);

			async function asyncRunner() {
				try {
					for (var i = 0; i < rows.length; i++) {
						//if (!rows[i].uid)
						//await updateIdP(tableName, rows[i].id, newIdColName);
						if (rows[i].idint == null) {
							await self.updateIdIntP(tableName, rows[i].id, ++maxId);
							console.log("updateIdForTable:" + i + ", " + rows[i].id + "," + maxId);
						}
					}
				} catch (error) {
					console.error(error);
				}
			}

			asyncRunner();
		});
	}

	async updateUserIdForTable(tableName: any, oldIdColName: string | number, newIdColName: any) {
		var self = this;
		var sql = `SELECT ${this.getSQLValue("id", SQLTYPE.NUMBER)}, ${oldIdColName} FROM ${tableName} `;

		var rows = await this.query(sql);

		async function asyncRunner() {
			try {
				for (var i = 0; i < rows.length; i++) {
					var intUserId = rows[i][oldIdColName as string];
					if (intUserId != null) {
						var user = self.myContext.userGetById2(intUserId + ""); //can't use userController here - circular deps
						if (user) {
							await self.updateIdP(tableName, rows[i].id, newIdColName, user.uid);
							console.log("updateIdForTable:" + i + ", " + user.id, ", " + user.uid);
						}
					}
				}
			} catch (error) {
				console.error(error);
			}
		}

		asyncRunner();
	}

	async getIdByIdInt(tableName: any, idInt: any, type: any, cb: any) {
		var sql = `SELECT ${this.getSQLValue("id", SQLTYPE.UUID)}   FROM ${tableName} t`;
		sql += " WHERE t.idint = " + this.setSQLValue(idInt, SQLTYPE.NUMBER);
		sql += " AND t.type = " + this.setSQLValue(type, SQLTYPE.NUMBER);

		var rows = await this.query(sql);

		if (rows.length > 0) {
			if (cb) {
				cb(rows[0]);
			}
		} else {
			cb(null);
		}
	}

	getIdByIdIntP(tableName: any, idInt: any, type: any): Promise<number> {
		return new Promise((resolve, reject) => {
			this.getIdByIdInt(tableName, idInt, type, (id: number | PromiseLike<number>) => {
				resolve(id);
			});
		});
	}
}
