import { Utils } from "../../public/src/shared/Utils.ts";
import { SQLTYPE, DbController } from "../db/DbController.ts";
import { MyContext } from "../MyContext.ts";

export class UserController {
	db: DbController;
	myContext: MyContext;

	constructor(db: DbController, myContext: MyContext) {
		this.db = db;
		this.myContext = myContext;
	}

	async userGetByName(name: any, cb: any) {
		var sql = `SELECT ${this.db.getSQLValue("id", SQLTYPE.UUID, "u")} as id, u.name as name FROM user u`;
		//var sql = `SELECT ${getSQLValue("id", SQLTYPE.NUMBER, "u") } as id, u.name as name FROM user u`;
		sql += ` WHERE u.name = '${name}'`;

		//console.error("userGetByName");

		if (this.db) {
			var rows = await this.db.query(sql);
			if (cb) {
				if (rows.length > 0) {
					//updateId("user", rows[0].id, "uid");
					cb(rows[0]);
				} else {
					cb(null);
				}
			}
		}
	}

	async userGetByNameBase(name: any) {
		var ret: any = null;

		var sql = `SELECT ${this.db.getSQLValue("id", SQLTYPE.UUID, "u")} as id, u.name as name FROM user u`;
		//var sql = `SELECT ${getSQLValue("id", SQLTYPE.NUMBER, "u") } as id, u.name as name FROM user u`;
		sql += ` WHERE u.name = '${name}'`;

		//console.error("userGetByName");

		if (this.db) {
			var rows = await this.db.query(sql);
			if (rows.length > 0) {
				ret = rows[0];
			}
		}
		return ret;
	}

	async userGetById(id: any, cb?: any) {
		//var sql = `SELECT ${getSQLValue("id", SQLTYPE.UUID, "u") } as id, u.name as name FROM user u`;
		var sql = `SELECT id, ${this.db.getSQLValue("uid", SQLTYPE.UUID, "u")} as uid, u.name as name FROM user u`;
		sql += ` WHERE u.id = '${id}'`;

		//console.error("userGetByName");

		var rows = await this.db.query(sql);
		if (cb) {
			if (rows.length > 0) {
				//updateId("user", rows[0].id, "uid");
				cb(rows[0]);
			} else {
				cb(null);
			}
		}
	}

	async userGetAll(cb?: ((arg0: any) => void) | undefined) {
		var self = this;
		//var sql = `SELECT  ${getSQLValue("uid", SQLTYPE.UUID)}, name, id FROM user u`;
		var sql = `SELECT  ${this.db.getSQLValue("id", SQLTYPE.UUID)}, name FROM user u`;

		//sql += ` WHERE u.name = '${name}'`;

		var rows = await this.db.query(sql);
		if (self.myContext && self.myContext.allUsers) {
			self.myContext.allUsers = rows as any;
		}
		if (cb && rows.length > 0) {
			cb(rows);
		}
	}

	async userGetNames(cb: any) {
		var sql = `SELECT  ${this.db.getSQLValue("id", SQLTYPE.UUID, "u")} as id, u.name as name FROM user u`;

		//sql += ` WHERE u.name = '${name}'`;

		var rows = await this.db.query(sql);
		if (cb && rows.length > 0) {
			cb(rows);
		}
	}
}
