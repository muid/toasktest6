//import sqlite from "sqlite3";
//import utils from "../../public/shared/utils"
import { Utils } from "../../public/src/shared/Utils.ts";
import { SQLTYPE, DbController } from "../db/DbController.ts";
import { MyContext } from "../MyContext.ts";

var allTags = [];

export class TagController {
	constructor(db: DbController, myContext: MyContext) {}
	tagGetItems(cb: any) {
		this.tagGetItemsForTodoItem(null, cb);
	}

	async tagGetItemsBase() {
		return this.tagGetItemsForTodoItemBase(null);
	}

	tagGetItemsForTodoItem(itemId: any, cb: any) {
		if (cb) {
			cb();
		}
	}
	async tagGetItemsForTodoItemBase(itemId: any) {
		var ret: any = null;

		return ret;
	}
}
/*
export class TagController {
	db: sqlite.Database;
	dbController: DbController;
	utils: Utils;
	constructor(db: sqlite.Database, myContext: MyContext) {
		this.db = db;
		this.dbController = new DbController(db, myContext);
		this.utils = new Utils();
		//this.repository = new AppAuthRepository(db.client);
	}

	tagGetItems(cb) {
		this.tagGetItemsForTodoItem(null, cb);
	}

	tagGetItemsForTodoItem(itemId, cb) {
		var sql = `SELECT 	${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id,\
							${this.dbController.getSQLValue("parentid", SQLTYPE.UUID, "t")} as parentid,\
						t.name as name \
						FROM tag t`;

		if (itemId) {
			sql += " INNER JOIN Tag2Todo t2t on t2t.idtag = t.id";
			if (this.utils.isUUID(itemId)) {
				sql += " WHERE t2t.idtodo = " + this.dbController.setSQLValue(itemId, SQLTYPE.UUID);
			} else {
				sql += " WHERE t2t.IdTodo = " + this.dbController.setSQLValue(itemId, SQLTYPE.NUMBER);
			}
		}

		sql += " ORDER BY name";

		this.db.all(sql, [], (err, rows: Array<any>) => {
			if (err) {
				console.error(err.message + ":" + sql);
			} else {
				//allTags = rows as any;
				if (cb) {
					cb(rows);
				}
			}
		});
	}

	tagAddOrUpdate(id, parentId, priority, color, name, cb?) {
		var sql = `INSERT INTO Tag(id, parentid, priority, color, name) VALUES(${this.dbController.setSQLValue(
			id,
			SQLTYPE.UUID
		)},${this.dbController.setSQLValue(parentId, SQLTYPE.NUMBER)},${this.dbController.setSQLValue(
			priority,
			SQLTYPE.NUMBER
		)},"${this.dbController.setSQLValue(color, SQLTYPE.TEXT)}","${this.dbController.setSQLValue(
			name,
			SQLTYPE.TEXT
		)}")`;

		if (id > 0) {
			sql = `UPDATE Tag SET name = '${this.dbController.setSQLValue(
				name,
				SQLTYPE.TEXT
			)}', parentId = '${this.dbController.setSQLValue(
				parentId,
				SQLTYPE.NUMBER
			)}', priority = '${this.dbController.setSQLValue(
				priority,
				SQLTYPE.NUMBER
			)}', color = '${this.dbController.setSQLValue(
				color,
				SQLTYPE.TEXT
			)}', name = '${this.dbController.setSQLValue(
				name,
				SQLTYPE.TEXT
			)}' WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;
		}

		this.db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			} else {
				this.tagGetItems((tags) => {
					this.tagExists(name, (tagId) => {
						if (cb) {
							cb(tagId);
						}
					});
				});
			}
		});
	}

	tagDel(id) {
		if (id) {
			var sql = "DELETE FROM tag WHERE id = " + id;

			this.db.run(sql, (err) => {
				if (err) {
					console.error(err.message);
				}
			});
		}
	}

	tagExists(tagName, cb) {
		var ret = undefined;

		if (allTags.length == 0) {
			this.tagGetItems((tags) => {
				ret = this.tagExistsBase(tagName);
				if (cb) {
					cb(ret);
				}
			});
		} else {
			ret = this.tagExistsBase(tagName);
			if (cb) {
				cb(ret);
			}
		}
	}

	tagExistsBase(tagName) {
		var ret = undefined;

		for (var key in allTags) {
			var tag = allTags[key] as any;

			if (tag.name == tagName) {
				ret = tag.id;
				break;
			}
		}

		return ret;
	}

	tag2TodoExists(todoId, tagId, cb) {
		var ret = -1;

		var sql = `SELECT ${this.dbController.getSQLValue("id", SQLTYPE.UUID)}, ${this.dbController.getSQLValue(
			"idtodo",
			SQLTYPE.UUID
		)} from Tag2Todo where idtag = ${this.dbController.setSQLValue(
			tagId,
			SQLTYPE.UUID
		)} AND idtodo =  ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}`;

		this.db.all(sql, [], (err, rows) => {
			if (err) {
				console.error(err.message);
			} else {
				if (rows && rows.length > 0) {
					ret = rows[0].Id;
				}

				if (cb) {
					cb(ret);
				}
			}
		});
	}

	tag2TodoAddDb(todoId, tagId) {
		var self = this;
		this.tag2TodoExists(todoId, tagId, (tag2TodoId) => {
			if (tag2TodoId == -1) {
				var sql = `INSERT INTO Tag2Todo(id, idtag, idtodo)\
				VALUES(${this.dbController.setSQLValue(self.dbController.uuidCreate(), SQLTYPE.UUID)}, ${this.dbController.setSQLValue(
					tagId,
					SQLTYPE.UUID
				)}, ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)})`;

				this.db.run(sql, (err) => {
					if (err) {
						console.error(err.message);
					}
				});
			}
		});
	}

	tag2TodoAdd(todoId, tagName) {
		this.tagExists(tagName, (tagId) => {
			if (tagId) {
				this.tag2TodoAddDb(todoId, tagId);
			} else {
				this.tagAddOrUpdate(null, 0, 0, "", tagName, (tagId) => {
					if (tagId) {
						this.tag2TodoAddDb(todoId, tagId);
					}
				});
			}
		});
	}

	tag2TodoDel(todoId, tagId) {
		this.tag2TodoExists(todoId, tagId, (tag2TodoId) => {
			var sql = `DELETE FROM tag2todo WHERE id = ${tag2TodoId}`;

			this.db.run(sql, (err) => {
				if (err) {
					console.error(err.message);
				} else {
				}
			});
		});
	}

	tag2TodoGetItems(cb) {
		var sql = `SELECT 	${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id,\
							${this.dbController.getSQLValue("idtag", SQLTYPE.UUID, "t")} as idtag,\
							${this.dbController.getSQLValue("idtodo", SQLTYPE.UUID, "t")} as idtodo \
					FROM tag2Todo t`;

		//if (itemId) {
		//	sql += ' INNER JOIN Tag2Todo t2t on t2t.IdTag = t.id';
		//	sql += ' WHERE t2t.IdTodo = '+itemId;
		//}

		//sql += ' WHERE t.iduser = '+ userId;

		//sql += " ORDER BY name";

		this.db.all(sql, [], (err, rows) => {
			if (err) {
				console.error(err.message);
			} else {
				//allTags = rows;
				if (cb) {
					cb(rows);
				}
			}
		});
	}
}
*/
