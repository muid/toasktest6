//import { Application, Request, Response } from "express";
//import sqlite from "sqlite3";
import { DbController, SQLTYPE } from "../db/DbController.ts";
import { UserController } from "../users/UserController.ts";
//import { TagController } from "../tags/TagController.t";
import { MyContext } from "../MyContext.ts";

var userController: UserController;
//var tagController: TagController;
var dbController: DbController;
/*
export function TagRoutes(app: Application, db: sqlite.Database, myContext: MyContext) {
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	dbController = new DbController(db, myContext);

	app.post("/api/v1/GetTagList", function (req, res) {
		var item = req.body;

		tagController.tagGetItems((tags) => {
			var json = JSON.stringify(tags);
			res.status(200).send(json);
		});
	});

	app.get("/tagAdd", (req, res) => {
		if (req.query["title"]) {
			var title = req.query["title"];
			tagController.tagAddOrUpdate(null, 0, 0, "", title);
		}

		return res.redirect("./todo");
	});

	app.get("/tagDel", (req, res) => {
		var id: number = 0;

		if (req.query["id"]) {
			id = parseInt(req.query["id"].toString());
			if (id == 0) {
				id = 0;
			}
		}

		if (id) {
			tagController.tagDel(id);
		}

		return res.redirect("./todo");
	});

	app.post("/todo/apiGetTag2TodoList", function (req, res) {
		var item = req.body;

		tagController.tag2TodoGetItems((tag2TodoList) => {
			var json = JSON.stringify(tag2TodoList);
			res.status(200).send(json);
		});
	});

	app.post("/todo/apiAddTag", function (req, res) {
		var item = req.body;

		if (item.name) {
			tagController.tagAddOrUpdate(null, null, 0, "", item.name, null);
		}

		res.status(200).send("");
	});

	app.post("/todo/apiDelTag", function (req, res) {
		var item = req.body;

		if (item.tagId != -1) {
			tagController.tagDel(item.tagId);
		}

		res.status(200).send("");
	});

	app.post("/todo/apiAddTagRelation", function (req, res) {
		var item = req.body;

		if (item.tagId == 0) {
			tagController.tag2TodoAdd(item.todoItemId, item.tagName);
		} else {
			tagController.tag2TodoAddDb(item.todoItemId, item.tagId);
		}

		res.status(200).send("");
	});

	app.post("/todo/apiDelTagRelation", function (req, res) {
		var item = req.body;

		if (item.tagId > 0) {
			tagController.tag2TodoDel(item.todoItemId, item.tagId);
		}

		res.status(200).send("");
	});
}
*/
