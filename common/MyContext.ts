import { Utils } from "../public/src/shared/Utils.ts";
export class MyContext {
	utils: Utils = new Utils();
	allUsers = [];

	userGetById2(userId: string): any {
		var ret = null;

		var user = this.utils.getById(this.allUsers, userId, "");
		if (user && user.id) {
			ret = user;
		}

		return ret;
	}
}
