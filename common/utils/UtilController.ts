//import sqlite from "sqlite3";
import { Utils } from "../../public/src/shared/Utils.ts";
import { SQLTYPE, DbController } from "../db/DbController.ts";
import { MyContext } from "../MyContext.ts";

export class UtilController {
	//db: sqlite.Database;
	dbController: DbController;
	utils: Utils;
	//implements BaseController<IUser>
	constructor(db: DbController, myContext: MyContext) {
		this.dbController = db;
		this.utils = new Utils();
		//this.dbController = new DbController(this.db, myContext);
		//this.repository = new AppAuthRepository(db.client);
	}

	writeCodedUrl(id: string, idInt: number, type: number) {
		var ret = "";

		ret = `§{${type}:${idInt}:${id}}`;

		return ret;
	}

	escapeText(text: string) {
		var ret = text.replace(/'/g, "#24");
		return ret;
	}

	replaceIdWithUrl(noteHTML: string, cb: any) {
		var self = this;
		var ret = noteHTML;

		async function asyncRunner() {
			try {
				for (var typeKey in self.utils.TODOTYPEDATA) {
					var key: string = typeKey;
					//var typeData = self.utils.TODOTYPEDATA[key];
					var typeData = { ID: 0, PREFIX: "TODO", IDLEN: 3 }; //todo debug
					var search = typeData.PREFIX + "0";
					for (var i = 0; i < 1; i++) {
						var index = ret.indexOf(search);
						if (index != -1) {
							var idLen = typeData.IDLEN + typeData.PREFIX.length;
							var partId = ret.substr(index, idLen);
							var partIdNr = partId.substr(typeData.PREFIX.length);
							var partIntId = parseInt(partIdNr);
							var part2 = ret.substr(index + idLen);
							var part1 = ret.substr(0, index);

							var res = await self.dbController.getIdByIdIntP("todo", partIntId, typeData.ID);
							if (res) {
								//var u = writeCodedUrl(res.id, partIntId, typeData.ID);
								var u = self.writeCodedUrl(res.toString(), partIntId, typeData.ID);

								ret = part1 + u + part2;
								i = -1;
								continue;
							}
							//var u = utils.getUrlForType(res.id,"muid", typeData.ID);

							//var what = "";
						}
					}
				}
			} catch (ex) {}

			if (cb) {
				cb(ret);
			}
		}

		asyncRunner();

		//return ret;
	}
}
