//import sqlite from "sqlite3";
//var https = require("https");
//var http = require("http");

import { SQLTYPE, DbController } from "../db/DbController.ts";
import { UserController } from "../users/UserController.ts";
import { TagController } from "../tags/TagController.ts";
import { UtilController } from "../utils/UtilController.ts";
import { MyContext } from "../MyContext.ts";
import { Utils } from "../../public/src/shared/Utils.ts";
import { TODOTYPE, TODOSTATUS, TODONOTESTATUS } from "../../public/src/shared/Types.ts";
export class BaseController {
	//	db: sqlite.Database;
	dbController: DbController;
	userController: UserController;
	tagController: TagController;
	utilController: UtilController;
	utils: Utils;

	//implements BaseController<IUser>
	constructor(db: DbController, myContext: MyContext) {
		this.dbController = db;
		//this.dbController = new DbController(this.db, myContext);
		this.userController = new UserController(this.dbController, myContext);
		this.tagController = new TagController(this.dbController, myContext);
		this.utilController = new UtilController(this.dbController, myContext);
		this.utils = new Utils();
	}

	getSQLTodoGetItems(userId: string, type: number, searchTerms: string[], tagBaseId: string, tagSearch: string, showDone: boolean, statusSet: number, todoIdsStr?: string) {
		var sql = `SELECT ${this.dbController.getSQLValueShort("id", SQLTYPE.UUID, "t")},`;
		sql += `${this.dbController.getSQLValueShort("idint", SQLTYPE.NUMBER, "t")},`;
		sql += `${this.dbController.getSQLValueShort("iduser", SQLTYPE.UUID, "t")},`;
		sql += `${this.dbController.getSQLValueShort("idnote", SQLTYPE.UUID, "t")},`;
		sql += `${this.dbController.getSQLValueShort("idgroup", SQLTYPE.UUID, "t")},`;
		sql += `${this.dbController.getSQLValueShort("type", SQLTYPE.NUMBER, "t")},`;
		sql += `t.name as name,`;
		sql += `n.note as notes,`;
		sql += `n.notehtml as notehtml,`;
		sql += `t.url as url,`;
		sql += `t.status as status,`;
		sql += `IFNULL(t.priority, 0) as priority,`;

		if (type == TODOTYPE.TASK) {
			//sql += `IFNULL(tg.priority, 0) as grouppriority,`;
			//sql += `IFNULL(tg.name, "") as groupname,`;
			//sql += `"" as grouppriority,`;
			//sql += `"" as groupname,`;
			sql += `IFNULL(t.done, 0) as grouppriority,`;
			sql += `IFNULL(t.done, 0) as groupname,`;
		}
		sql += `IFNULL(t.done, 0) as done,`;
		//sql += `IFNULL(t.status,0) as status,`;
		sql += `t.datedue as datedue,`;
		sql += `${this.dbController.getSQLValueShort("idusercreated", SQLTYPE.UUID, "t")},`;
		sql += `t.datecreated as datecreated,`;
		sql += `${this.dbController.getSQLValueShort("idusermodified", SQLTYPE.UUID, "t")},`;
		sql += `t.datemodified as datemodified`;
		sql += ` FROM todo t`;

		sql += " LEFT JOIN note n on n.id = t.idnote";
		/*if (tagBaseId > 0) {
			sql += "\nINNER JOIN Tag2Todo tt on t.id = tt.idtodo\n";
		}*/

		if (type == TODOTYPE.TASK) {
			sql += " LEFT JOIN taskgroup tg on tg.id = t.idgroup";
		}

		if (todoIdsStr && todoIdsStr.length > 0) {
			sql += ` WHERE t.id in (${todoIdsStr})`;
		} else {
			sql += " WHERE 1=1 ";
		}

		var searchSQL = this.dbController.getSQLLikeTerm("t.name", searchTerms, false);
		if (searchSQL && searchSQL.length > 0) {
			sql += " AND " + searchSQL;
		}

		/*if (tagBaseId > 0) {
			sql += " AND (tt.idtag = " + tagBaseId;

			if (tagSearch > 0) {
				sql += " OR tt.idtag = " + tagSearch;
			}

			sql += " ) ";
		}*/

		if (type != TODOTYPE.TASK) {
			sql += ` AND t.iduser = ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}`;
		}

		if (!statusSet && type != TODOTYPE.TASK) {
			/*status = statusSet;
            sql += ` AND t.status = ${status}`;*/
			sql += ` AND (t.status = 0 or t.status is null or t.status = 'null')`;
		} else {
		}

		if (!showDone) {
			sql += " AND (t.done = 0 or t.done is null)";
			sql += " AND (t.status != 20)";
		}

		if (type) {
			sql += ` AND t.type = ${type}`;
		} else {
			sql += ` AND IFNULL(t.type, 0) == 0`;
		}

		if (type == TODOTYPE.JOURNAL) {
			sql += " ORDER BY IFNULL(t.datedue,0) DESC";
		} else if (type == TODOTYPE.KB) {
			sql += " ORDER BY IFNULL(t.datemodified,0) DESC";
		} else if (type == TODOTYPE.TODO) {
			sql += " ORDER BY IFNULL(t.done,0) ASC, IFNULL(t.status, 0) ASC, IFNULL(t.priority,0) DESC, t.name";
		} /* else if (type == TODOTYPE.TASK) {
			sql += " ORDER BY IFNULL(tg.priority,0) DESC, IFNULL(t.priority,0) DESC, t.name";
		}*/

		return sql;
	}

	async todoGetItems(userId: string, type: number, searchTerms: string[], tags: string[], tagBaseId: string, tagSearch: string, showDone: boolean, statusSet: number /*, cb: any*/) {
		var sql = this.getSQLTodoGetItems(userId, type, searchTerms, tagBaseId, tagSearch, showDone, statusSet);

		var rows = await this.dbController.query(sql);
		/*	if (cb) {
			cb(rows, tags);
		}*/
		return rows;
	}

	async todoGetItem(todoId: string, cb: any) {
		var sql = `SELECT ${this.dbController.getSQLValue("id", SQLTYPE.UUID)}, ${this.dbController.getSQLValue("idint", SQLTYPE.NUMBER)}, ${this.dbController.getSQLValue("idnote", SQLTYPE.UUID)}, ${this.dbController.getSQLValue(
			"idgroup",
			SQLTYPE.UUID
		)}, t.name as name, t.notes as notes, t.url as url, t.type as type, t.done as done, t.status as status, t.priority as priority, t.datedue as datedue, t.idusercreated as idusercreated, t.datecreated as datecreated, t.idusermodified as idusermodified, t.datemodified as datemodified  FROM todo t`;

		if (this.utils.isUUID(todoId)) {
			sql += " WHERE t.id = " + this.dbController.setSQLValue(todoId, SQLTYPE.UUID);
		} else {
			sql += " WHERE t.idint = " + this.dbController.setSQLValue(todoId, SQLTYPE.NUMBER);
		}

		var rows = await this.dbController.query(sql);
		if (rows.length > 0) {
			if (!rows[0].id) {
				var d = ";";
				//updateId("todo", rows[0].id);
			}

			if (cb) {
				cb(rows[0]);
			}
		} else {
			cb();
		}
	}

	async todoGetItemBase(todoId: string) {
		var ret: any = null;

		var sql = `SELECT ${this.dbController.getSQLValue("id", SQLTYPE.UUID)}, ${this.dbController.getSQLValue("idint", SQLTYPE.NUMBER)}, ${this.dbController.getSQLValue("idnote", SQLTYPE.UUID)}, ${this.dbController.getSQLValue(
			"idgroup",
			SQLTYPE.UUID
		)}, t.name as name, t.notes as notes, t.url as url, t.type as type, t.done as done, t.status as status, t.priority as priority, t.datedue as datedue, t.idusercreated as idusercreated, t.datecreated as datecreated, t.idusermodified as idusermodified, t.datemodified as datemodified  FROM todo t`;

		if (this.utils.isUUID(todoId)) {
			sql += " WHERE t.id = " + this.dbController.setSQLValue(todoId, SQLTYPE.UUID);
		} else {
			sql += " WHERE t.idint = " + this.dbController.setSQLValue(todoId, SQLTYPE.NUMBER);
		}

		var rows = await this.dbController.query(sql);
		if (rows.length > 0) {
			ret = rows[0];
		}
		return ret;
	}

	todoAddOrUpdate(id: string, userId: string, noteId: string, groupId: string, type: number, name: string, notes: string, url: string, done: boolean, priority: number, status: number, duedate: number, cb: any) {
		this.dbController.getMaxIdInt("todo", type, (maxId) => {
			var todoId = id;
			var date = new Date().getTime();

			if (!type) {
				type = TODOTYPE.TODO;
			}

			if (name) {
				if (this.utils.isUUID(id) && !this.utils.isUUIDEmpty(id)) {
					sql = `UPDATE todo `;

					sql += `SET iduser = ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}`;
					if (noteId) {
						sql += `, idnote = ${this.dbController.setSQLValue(noteId, SQLTYPE.UUID)}`;
					}
					sql += `, idgroup = ${this.dbController.setSQLValue(groupId, SQLTYPE.UUID)}`;
					sql += `, name = '${this.dbController.setSQLValue(name, SQLTYPE.TEXT)}'`;
					//sql += `, notes = '${sqlValue(notes, SQLTYPE.TEXT)}'`;
					sql += `, url = '${this.dbController.setSQLValue(url, SQLTYPE.TEXT)}'`;
					sql += `, priority = '${this.dbController.setSQLValue(priority, SQLTYPE.NUMBER)}'`;
					sql += `, done = '${this.dbController.setSQLValue(done, SQLTYPE.NUMBER)}'`;
					sql += `, status = '${this.dbController.setSQLValue(status, SQLTYPE.NUMBER)}'`;
					sql += `, type = '${this.dbController.setSQLValue(type, SQLTYPE.NUMBER)}'`;
					sql += `, datedue = '${this.dbController.setSQLValue(duedate, SQLTYPE.NUMBER)}'`;
					sql += `, idusermodified = ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}`;
					sql += `, datemodified = '${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)}'`;

					sql += ` WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;
				} else {
					todoId = this.dbController.uuidCreate();
					var sql = `INSERT INTO todo(id, idint, iduser, idnote, type, name, url, priority, done, status, datedue, idusercreated, datecreated, idusermodified, datemodified) 
					VALUES(${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}, ${this.dbController.setSQLValue(maxId + 1, SQLTYPE.NUMBER)},  ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}, ${this.dbController.setSQLValue(
						noteId,
						SQLTYPE.UUID
					)}, ${this.dbController.setSQLValue(type, SQLTYPE.NUMBER)},'${this.dbController.setSQLValue(name, SQLTYPE.TEXT)}','${this.dbController.setSQLValue(url, SQLTYPE.TEXT)}',${this.dbController.setSQLValue(
						priority,
						SQLTYPE.NUMBER
					)}, ${this.dbController.setSQLValue(done, SQLTYPE.NUMBER)}, ${this.dbController.setSQLValue(status, SQLTYPE.NUMBER)}, ${this.dbController.setSQLValue(duedate, SQLTYPE.NUMBER)}, ${this.dbController.setSQLValue(
						userId,
						SQLTYPE.UUID
					)}, ${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)}, ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}, ${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)})`;
				}

				var rows = this.dbController.query(sql);

				/*
				if (todoId == -1) {
					todoId = this.lastID;
				}*/
				/*
				for (tag in tags) {
					tag2TodoAdd(todoId, tag);
					sql = 'INSERT INTO Tag2Todo(null, done, name)\
					VALUES(' + id + ',' + done+ ',"' + name +  '")';
				}*/

				if (cb) {
					cb(todoId);
				}
			} else {
				if (cb) {
					cb(-1);
				}
			}
		});
	}

	todoDel(id: string) {
		if (id) {
			var sql = `DELETE FROM todo WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

			var rows = this.dbController.query(sql);
		}
	}

	todoSetStatus(id: string, status: number) {
		if (id) {
			var sql = `UPDATE todo SET status = ${status} WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

			var rows = this.dbController.query(sql);
		}
	}

	getPropertyListFromObjectArray(arr: any[], memberName: string) {
		var ret = Array<any>();

		for (var i = 0; i < arr.length; i++) {
			var val = arr[i][memberName];
			ret.push(val);
		}

		return ret;
	}

	getIdsListFromArr(tagIdsArr: any[], prefix: string) {
		var ret = "";

		if (tagIdsArr) {
			for (var i = 0; i < tagIdsArr.length; i++) {
				ret += `${prefix}'${tagIdsArr[i]}',`;
			}

			if (ret.length > 0) {
				ret = ret.substr(0, ret.length - 1);
			}
		}

		return ret;
	}

	todoGetItems2(userId: string, type: number, searchTerms: string[], idTags: any, idTagsExclude: any, showDone: boolean, statusSet: number, cb: any) {
		this.todoGetItemIdsForTags(idTags, idTagsExclude, (todoIds: any[]) => {
			var todoIdsArr = this.getPropertyListFromObjectArray(todoIds, "id");
			var todoIdsStr = this.getIdsListFromArr(todoIdsArr, "X");
			if (todoIdsStr) {
				var sql = this.getSQLTodoGetItems(userId, type, searchTerms, "", "", showDone, statusSet, todoIdsStr);

				var rows = this.dbController.query(sql);
				if (cb) {
					cb(rows);
				}
			} else {
				if (cb) {
					cb([]);
				}
			}
		});
	}

	todoGetItemIdsForTags(tagIds: any[], tagIdsExclude: any[], cb: any) {
		var tagIdsStr = "";
		var tagIdsExcludeStr = "";
		if (tagIds) {
			tagIdsStr = this.getIdsListFromArr(tagIds, "X");
		}

		if (tagIdsExclude) {
			tagIdsExcludeStr = this.getIdsListFromArr(tagIdsExclude, "X");
		}

		var sql = `select distinct ${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id from todo t`;
		//sql+= ` inner join tag2todo t2t on t.id = t2t.idtodo`;
		sql += ` where `;

		if (tagIds) {
			var subSelect = `select idtodo from Tag2Todo t2t where t2t.idtag in (${tagIdsStr})`;
			sql += ` t.id in (${subSelect})`;
		}

		if (tagIds && tagIdsExclude) {
			sql += ` and`;
		}

		if (tagIdsExclude) {
			var subSelect = `select idtodo from Tag2Todo t2t where t2t.idtag in (${tagIdsExcludeStr})`;
			sql += ` t.id not in (${subSelect})`;

			//sql += ` t2t.idtag not in (${tagIdsExcludeStr})`;
		}

		var rows = this.dbController.query(sql);

		if (cb) {
			cb(rows);
		}
	}

	todoGetItemP(id: string, cb: any): Promise<void> {
		return new Promise((resolve, reject) => {
			return this.todoGetItem(id, (item: any) => {
				if (cb) {
					cb(item);
				}

				resolve();
			});
		});
	}

	tag2TodoGetItems(cb: any) {
		var sql = `SELECT 	${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id,\
						${this.dbController.getSQLValue("idtag", SQLTYPE.UUID, "t")} as idtag,\
						${this.dbController.getSQLValue("idtodo", SQLTYPE.UUID, "t")} as idtodo \
				FROM tag2Todo t`;

		/*if (itemId) {
		sql += ' INNER JOIN Tag2Todo t2t on t2t.IdTag = t.id';
		sql += ' WHERE t2t.IdTodo = '+itemId;
	}*/

		//sql += ' WHERE t.iduser = '+ userId;

		//sql += " ORDER BY name";

		var rows = this.dbController.query(sql);
		if (cb) {
			cb(rows);
		}
	}

	noteUpdateText(id: string, noteText: string, noteHTML: string, userId: string, cb: any) {
		var date = new Date().getTime();

		if (noteText || noteHTML) {
			if (!noteHTML) {
				noteHTML = noteText;
			}

			this.utilController.replaceIdWithUrl(noteHTML, (noteHTMLnew: string) => {
				var sql = `UPDATE note `;
				sql += `SET note = '${this.dbController.setSQLValue(noteText, SQLTYPE.TEXT)}'`;
				sql += `, notehtml = '${this.dbController.setSQLValue(noteHTMLnew, SQLTYPE.TEXT)}'`;
				sql += `, idusermodified = ${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}`;
				sql += `, datemodified = '${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)}'`;

				sql += ` WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

				var rows = this.dbController.query(sql);

				if (cb) {
					cb(id);
				}
			});
		} else {
			if (cb) {
				cb(-1);
			}
		}
	}

	noteAddOrUpdate(idobsolete: string, idfk: string, userId: string, type: number, status: number, note: string, noteHTML: string, cb: any) {
		var self = this;
		var date = new Date().getTime();
		var lastId = this.dbController.uuidCreate();

		if (note || noteHTML) {
			if ((noteHTML && noteHTML.length == 0) || !noteHTML) {
				noteHTML = note;
			}

			this.utilController.replaceIdWithUrl(noteHTML, (noteHTMLnew: string) => {
				var sql = `INSERT INTO note(id, idfk, type, status, note, notehtml, idusercreated, datecreated, idusermodified, datemodified) 
				VALUES(	${this.dbController.setSQLValue(lastId, SQLTYPE.UUID)}, 
						${this.dbController.setSQLValue(idfk, SQLTYPE.UUID)}, 
						${this.dbController.setSQLValue(type, SQLTYPE.NUMBER)}, 
						${this.dbController.setSQLValue(status, SQLTYPE.NUMBER)},
						'${this.dbController.setSQLValue(note, SQLTYPE.TEXT)}',
						'${this.dbController.setSQLValue(noteHTMLnew, SQLTYPE.TEXT)}', 
						${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}, 
						${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)}, 
						${this.dbController.setSQLValue(userId, SQLTYPE.UUID)}, 
						${this.dbController.setSQLValue(date, SQLTYPE.NUMBER)})`;
				/*
				if (id != -1) {
					sql = `UPDATE note `;
					sql += `SET idfk = '${sqlValue(idfk, SQLTYPE.NUMBER)}'`;
		
					sql += `, type = '${sqlValue(type, SQLTYPE.TEXT)}'`;
					sql += `, status = '${sqlValue(status, SQLTYPE.NUMBER)}'`;
					sql += `, note = '${sqlValue(note, SQLTYPE.TEXT)}'`;
					sql += `, notehtml = '${sqlValue(noteHTML, SQLTYPE.TEXT)}'`;
					sql += `, idusermodified = '${sqlValue(userId, SQLTYPE.NUMBER)}'`;
					sql += `, datemodified = '${sqlValue(date, SQLTYPE.NUMBER)}'`;
		
					sql += ` WHERE id = '${id}'`;
				}*/

				var rows = this.dbController.query(sql);

				//lastId = this.lastID;

				self.todoUpdateNoteStatus(idobsolete, TODONOTESTATUS.OBSOLETE, () => {
					//todoUpdateNoteId(idfk, lastId, () => {
					if (cb) {
						cb(lastId);
					}
					//});
				});
			});
		} else {
			self.todoUpdateNoteStatus(idobsolete, status, () => {
				if (cb) {
					cb(-1);
				}
			});
		}
	}

	todoUpdateNoteStatus(id: string, status: number, cb: any) {
		if (id) {
			var sql = `UPDATE note `;

			sql += `SET status = '${this.dbController.setSQLValue(status, SQLTYPE.NUMBER)}' WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

			var rows = this.dbController.query(sql);
			if (cb) {
				cb();
			}
		} else {
			if (cb) {
				cb();
			}
		}
	}

	todoUpdateNoteId(id: string, noteId: string, type: number) {
		var sql = `UPDATE todo `;

		sql += `SET idnote = ${this.dbController.setSQLValue(noteId, SQLTYPE.UUID)} WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

		var rows = this.dbController.query(sql);
	}

	noteUpdateTodoId(id: string, todoId: string, type: any, cb?: any) {
		var sql = `UPDATE note `;

		if (type == TODOTYPE.TODO) {
			sql += `SET idtodo = ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}`;
		} else if (type == TODOTYPE.KB) {
			sql += `SET idkb = ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}`;
		} else if (type == TODOTYPE.JOURNAL) {
			sql += `SET idjournal = ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}`;
		} else if (type == TODOTYPE.TASK) {
			sql += `SET idtask = ${this.dbController.setSQLValue(todoId, SQLTYPE.UUID)}`;
		}

		sql += ` WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;

		var rows = this.dbController.query(sql);
		if (cb) {
			cb();
		}
	}

	async noteGetItemsFor(id: string /*, cb: any*/) {
		return await this.noteGetItemsBase("", id);
	}

	async noteGetItemsForP(id: string) {
		return await this.noteGetItemsBase("", id);
	}
	/*
	noteGetItemsForP(id: string, cb: any): Promise<void> {
		return new Promise((resolve, reject) => {
			return this.noteGetItemsBase("", id, (notes: string) => {
				if (cb) {
					cb(notes);
				}

				resolve();
			});
		});
	}
*/
	async noteGetItem(id: string /*, cb: any*/) {
		var rows: Array<any> = await this.noteGetItemsBase(id, "");
		var ret = null;
		if (rows && rows.length > 0) {
			ret = rows[0];
		}
	}

	async noteGetItemsBase(id: string, idfk: string /*, cb: any*/) {
		var sql = `SELECT 	${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id,`;
		sql += `${this.dbController.getSQLValue("idfk", SQLTYPE.UUID, "t")} as idfk,`;
		sql += `${this.dbController.getSQLValue("idtodo", SQLTYPE.UUID, "t")} as idtodo,`;
		sql += `${this.dbController.getSQLValue("idjournal", SQLTYPE.UUID, "t")} as idjournal,`;
		sql += `${this.dbController.getSQLValue("idkb", SQLTYPE.UUID, "t")} as idkb,`;
		sql += `${this.dbController.getSQLValue("idtask", SQLTYPE.UUID, "t")} as idtask,`;
		sql += `${this.dbController.getSQLValue("status", SQLTYPE.NUMBER, "t")} as status,`;
		sql += `${this.dbController.getSQLValue("note", SQLTYPE.TEXT, "t")} as note,`;
		sql += `${this.dbController.getSQLValue("notehtml", SQLTYPE.TEXT, "t")} as notehtml,`;
		sql += `${this.dbController.getSQLValue("idusercreated", SQLTYPE.UUID, "t")} as idusercreated,`;
		sql += `${this.dbController.getSQLValue("datecreated", SQLTYPE.NUMBER, "t")} as datecreated,`;
		sql += `${this.dbController.getSQLValue("idusermodified", SQLTYPE.UUID, "t")} as idusermodified,`;
		sql += `${this.dbController.getSQLValue("datemodified", SQLTYPE.NUMBER, "t")} as datemodified`;
		sql += ` FROM note t`;
		if (id || idfk) {
			sql += ` WHERE `;
			if (idfk) {
				sql += ` idfk = ${this.dbController.setSQLValue(idfk, SQLTYPE.UUID)}`;
			} else {
				sql += ` id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;
			}
		}

		if (idfk) {
			sql += ` AND status not in (${TODONOTESTATUS.OBSOLETE},${TODONOTESTATUS.DELETED})`;
		}

		sql += " ORDER BY datecreated DESC";

		var rows = await this.dbController.query(sql);
		return rows;
		/*if (cb) {
			cb(rows);
		}*/
	}

	getTodoNotes(data: any, preferText: boolean) {
		//replace \n with <br>
		var ret = "";

		if (data) {
			ret = data.notehtml;

			if (preferText) {
				ret = data.note;
			}

			if (!ret) {
				ret = data.note;

				if (preferText && data.notehtml) {
					ret = data.notehtml;
				}
			}
		}

		if (!ret) {
			ret = "";
		}

		return ret;
	}

	/*GetTitleFromUrl(url: string, forceHttps: boolean, cb: any) {
		var ret = "";

		if (!this.utils.isUrl(url)) {
			cb("");
			return;
		}

		var buff = Buffer.alloc(0);

		var httpFunc = http.get;
		if (forceHttps || url.indexOf("https:") != -1) {
			httpFunc = https.get;
		}

		if (forceHttps) {
			if (url.indexOf("https:") == -1) {
				var index = url.indexOf("http:");
				if (index != -1) {
					url = "https:" + url.substr(5);
				} else {
				}
			}
		}

		httpFunc(url, function (res: any) {
			var pass = 1;
			var lastPass = false;
			var maxSize = 100000;
			if (url.toLowerCase().indexOf("amazon") != -1 || url.toLowerCase().indexOf("youtube") != -1) {
				maxSize = 3000000;
			}

			res.on("data", function (chunk: any) {
				buff = Buffer.concat([buff, chunk]);
				var tmpChunkStr = chunk.toString().toLowerCase();
				var tmpHtmlStr = buff.toString().toLowerCase();
				var titleFound = false;
				var htmlFound = false;
				var rHtml = /<html(\s|.)*?>/;
				var rTitle = /<title>/;
				if (rHtml.test(tmpHtmlStr) || htmlFound) {
					htmlFound = true;

					if (rTitle.test(tmpHtmlStr)) {
						if (tmpHtmlStr.length < 5000) {
							if (tmpHtmlStr.indexOf("301") == -1 && tmpHtmlStr.indexOf("moved") == -1 && tmpHtmlStr.indexOf("permanently") == -1) {
								titleFound = true;
							}
						} else {
							titleFound = true;
						}

						res.destroy();
						lastPass = true;
					}
				}

				if (titleFound) {
					var tmpHtmlStr = tmpHtmlStr.replace(/[\n\r]/g, " ");
					var m = tmpHtmlStr.match(/<title>((.*))<\/title>/g);
					if (m && m.length > 0) {
						ret = m[0];
						ret = ret.replace("<title>", "");
						ret = ret.replace("</title>", "");
						ret = ret.trim();
					}
					//ret = "titleatitle";
				} else if (pass == 1) {
					if (!rHtml.test(tmpHtmlStr)) {
						res.destroy();
						lastPass = true;
					}
				} else {
					if (!lastPass && tmpHtmlStr.length > maxSize) {
						res.destroy();
						lastPass = true;
					}
				}

				pass++;

				if (titleFound || lastPass) {
					cb(ret);
				}
			});
		});

		return ret;
	}*/
}
