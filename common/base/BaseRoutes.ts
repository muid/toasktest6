//import { Application, Request, Response } from "express";
//import sqlite from "sqlite3";
import { Router, Context, RouterContext } from "../../deps/oak.ts";
import { DbController, SQLTYPE } from "../db/DbController.ts";
//import { UserController } from "../users/UserController";
import { UserController } from "../users/UserController.ts";
import { TagController } from "../tags/TagController.ts";
import { BaseController } from "./BaseController.ts";
import { TODOSTATUS, TODONOTESTATUS } from "../../public/src/shared/Types.ts";
import { MyContext } from "../MyContext.ts";

var userController: UserController;
var tagController: TagController;
var dbController: DbController;
var baseController: BaseController;
//export function use(path: string, app: Router, userController: UserController) {
export function use(app: Router, db: DbController, myContext: MyContext) {
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	//dbController = new DbController(db, myContext);
	dbController = db;
	baseController = new BaseController(db, myContext);

	app.post("/api/v1/GetUserList", (ctx: RouterContext) => {
		userController.userGetNames((users: any) => {
			var json = JSON.stringify(users);
			ctx.response.status = 200;
			ctx.response.body = json;
		});
	});

	app.post("/api/v1/GetTodoItemList", async (ctx: Context) => {
		//const body = await ctx.request.body();
		const body = ctx.request.body();
		const params = await body.value;

		//var params: any; // = ctx.params;
		var userId = params.userId ?? "";
		var showDone = false;
		if (params.showDone) {
			//showDone =  parseInt(params.showDone);
		}
		var type = 0;
		if (params.type) {
			type = parseInt(params.type);
		}

		var statusSet = 0;
		if (params.showDel) {
			statusSet = TODOSTATUS.INACTIVE;
		}

		var tagBase = params.tagBase ?? "";
		var tagSearch = params.tagSearch ?? "";
		var tags: any[] = [];
		var searchTerms: string[] = [];
		if (params.searchTerm && params.searchTerm.length > 0) {
			searchTerms = params.searchTerm.split(" ");
		}

		var todoItems = await baseController.todoGetItems(userId, type, searchTerms, tags, tagBase, tagSearch, showDone, statusSet); //, (todoItems: any[], tags: any[]) => {
		var json = JSON.stringify(todoItems);
		ctx.response.status = 200;
		ctx.response.body = json;
		//});
	});

	app.post("/api/v1/GetTodoItemListByTags", function (ctx: RouterContext) {
		var params = ctx.params;
		var userId = params.userId ?? "";
		var showDone = params.showDone ? true : false;
		var type = parseInt(params.type ?? "".toString());

		var statusSet = 0;
		if (params.showDel) {
			statusSet = TODOSTATUS.INACTIVE;
		}

		var tagIds = params.tagIds;
		var tagIdsExclude = params.tagIdsExclude;
		var searchTerms: string[] = [];
		if (params.searchTerm && params.searchTerm.length > 0) {
			searchTerms = params.searchTerm.split(" ");
		}

		baseController.todoGetItems2(userId, type, searchTerms, tagIds, tagIdsExclude, showDone, statusSet, (todoItems: any[]) => {
			var json = JSON.stringify(todoItems);
			ctx.response.status = 200;
			ctx.response.body = json;
		});
	});

	app.post("/api/v1/GetTodoItemById", async function (ctx: Context) {
		const body = ctx.request.body();
		const params = await body.value;

		var itemId = params.itemId ?? "";
		baseController.todoGetItem(itemId, (todoItem: any) => {
			var note: any = baseController.noteGetItem(todoItem.idnote); //, (note: any) => {
			todoItem.notes = "";
			todoItem.notehtml = "";
			if (note) {
				todoItem.notes = note.note;

				if ((note.notehtml && note.notehtml.length == 0) || !note.notehtml) {
					todoItem.notehtml = note.note;
				} else {
					todoItem.notehtml = note.notehtml;
				}
			}
			var json = JSON.stringify(todoItem);
			ctx.response.status = 200;
			ctx.response.body = json;
		});
		//});
	});

	app.post("/api/v1/GetNotesByTodoId", async function (ctx: Context) {
		const body = ctx.request.body();
		const params = await body.value;
		const itemId = params.itemId;

		var noteItems = baseController.noteGetItemsFor(itemId); //, (noteItems: any[]) => {
		var json = JSON.stringify(noteItems);
		ctx.response.status = 200;
		ctx.response.body = json;
		//});
	});
	/*
	app.post("/api/v1/GetTag2TodoList", function (ctx: RouterContext) {
		var item = req.body;

		baseController.tag2TodoGetItems((tag2TodoList: any[]) => {
			var json = JSON.stringify(tag2TodoList);
			ctx.response.status = 200;ctx.response.body = json;
		});
	});
*/
	app.post("/api/v1/Upd", async function (ctx: Context) {
		const body = ctx.request.body();
		const item: any = await body.value;

		baseController.todoAddOrUpdate(item.id, item.userid, "", item.idgroup, item.type, item.name, item.notes, item.url, item.done, item.priority, item.status, item.duedate, (lastId: string) => {
			if (lastId) {
				if (item.notes && item.notes.length > 0) {
					baseController.noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, (lastIdNote: string) => {
						//as we always add a new note instead of updating the existing note -> mark the existing note as obsolete
						baseController.todoUpdateNoteStatus(lastIdNote, TODONOTESTATUS.OBSOLETE, () => {
							baseController.todoUpdateNoteId(lastId, lastIdNote, 0);
						});
						ctx.response.status = 200;
						ctx.response.body = dbController.uuidToShortUuid(lastId);
					});
				} else {
					ctx.response.status = 200;
					ctx.response.body = dbController.uuidToShortUuid(lastId);
				}

				/*
				noteAddOrUpdate(-1, lastId, item.userid, 1, 0, item.notes, item.notesHTML, (lastIdNote) => {
					todoUpdateNoteStatus(lastIdNote, TODONOTESTATUS.OBSOLETE, () => {
						todoUpdateNoteId(lastId, lastIdNote, () => {
							ctx.response.status = 200;.send('');	
						});
					});
					
				});*/
			} else {
				ctx.response.status = 200;
				ctx.response.body = "";
			}
		});
	});

	app.post("/api/v1/NoteUpd", async function (ctx: RouterContext) {
		const body = ctx.request.body();
		const item: any = await body.value;

		baseController.noteAddOrUpdate(item.idnote, item.idfk, item.userid, 1, item.status, item.notes, item.notehtml, () => {
			ctx.response.status = 200;
			ctx.response.body = "";
		});
	});
}
