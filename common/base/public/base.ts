import { Utils } from "../../../public/src/shared/Utils.ts";

var ko: any = {};

export enum TODOTYPE {
	TODO = 0,
	JOURNAL,
	KB,
	UNSORTED,
	TASK,
}

export enum TODOSTATUS {
	ACTIVE = 0,
	INACTIVE,
	ARCHIVED,
}

export enum TODONOTESTATUS {
	ACTIVE = 0,
	OBSOLETE,
	DELETED,
}

export class ViewModelNotesBase {
	utils: Utils;
	constructor(idTodoItem: any) {
		this.todoItemId = ko.observable(idTodoItem);
		this.utils = new Utils();
	}

	todoItemId = ko.observable("");
	noteItems = ko.observableArray([]);

	getDateTimeStrFromMS(ms: string | number | Date) {
		return this.utils.getDateTimeStrFromMS(ms);
	}

	createIdStrForIdInt(idInt: string | number, type: number) {
		return this.utils.createIdStrForIdInt(idInt, type);
	}
	getTaskNotes(taskId: any, cb: any) {
		var req = {
			itemId: taskId,
		};

		xhrPost("/api/v1/GetNotesByTodoId", ko.toJSON(req), true, (json: any) => {
			if (cb) {
				cb(json);
			}
		});
	}

	addUpdateTaskNote(data: any, event: any) {
		var self = this;

		addUpdateNote(data, () => {
			self.getTaskNotes(data.idfk, (notes: any) => {
				this.noteItems(notes);
			});
		});
	}

	delNote(data: any, event: any) {
		data.status = TODONOTESTATUS.DELETED;
		this.addUpdateNote(data);
	}

	addUpdateNote(noteItem: { id: () => any; idfk: () => any; userid: () => any; status: any }) {
		var self = this;

		var req = {
			idnote: typeof noteItem.id == "function" ? noteItem.id() : noteItem.id,
			idfk: typeof noteItem.idfk == "function" ? noteItem.idfk() : noteItem.idfk,
			iduser: typeof noteItem.userid == "function" ? noteItem.userid() : noteItem.userid, //,
			status: noteItem.status,
			notes: "",
			notehtml: "",
		};

		addUpdateNote(req, () => {
			self.getTaskNotes(req.idfk, (notes: any) => {
				this.noteItems(notes);
			});
		});
	}
}

export class ViewModelEditBase {
	utils: Utils;
	constructor() {
		this.utils = new Utils();
	}
	id = ko.observable("");
	idint = ko.observable(-1);
	userList = ko.observable([]);
	userid = ko.observable("");
	idgroup = ko.observable("");
	idintgroup = ko.observable(-1);
	groupname = ko.observable("");
	username = ko.observable("");
	type = ko.observable(0);
	status = ko.observable("");
	name = ko.observable("");
	url = ko.observable("");
	priority = ko.observable(0);
	done = ko.observable(false);
	notes = ko.observable("");
	idnote = ko.observable("");
	notehtml = ko.observable("");
	//self.taskGroupItems = ko.observable(undefined);
	taskGroupItems = ko.observableArray([]);
	inpCapture = ko.observable(null);
	/*
    self.addUpdateTaskNote = function(data, event) {
        addUpdateTaskNote(data);
    };*/

	setData(id: any, idint: any, userid: any, idgroup: any, username: string, status: any, name: any, url: any, priority: any, done: any, notes: any, type: any) {
		this.id(id);
		this.idint(idint);
		this.userid(userid);
		this.idgroup(idgroup);
		this.idintgroup(-1);
		this.groupname("");
		this.username(username);
		this.type(type);
		this.status(status);
		this.name(name);
		this.url(url);
		this.priority(priority);
		this.done(done);
		this.notes(notes);
		//this.taskGroupItems()
		//this.inpCapture =
	}

	getItem(taskId: any, setData: any, cb: (arg0: any) => void) {
		var req = {
			itemId: taskId,
		};

		xhrPost("/api/v1/GetTodoItemById", ko.toJSON(req), true, (json: any) => {
			if (setData) {
				var taskItem = json;
				this.setData(taskItem.id, taskItem.idint, this.userid(), taskItem.idgroup, "", taskItem.status, taskItem.name, taskItem.url, taskItem.priority, taskItem.done, taskItem.notes, taskItem.type);
			}
			if (cb) {
				cb(json);
			}
		});
	}

	saveItem(type: any, cb: (arg0: any) => void) {
		var req = {
			id: this.id,
			userid: this.userid,
			idnote: this.idnote,
			idgroup: this.idgroup,
			type: type,
			name: this.name,
			notes: this.notes,
			notehtml: this.notehtml,
			done: this.done,
			priority: this.priority,
			status: this.status,
			url: this.url,
		};

		xhrPost("/api/v1/Upd", ko.toJSON(req), false, (json: any) => {
			if (cb) {
				cb(json);
			}
		});

		return true;
		/*
        xhrPost('/todo/apiUpd', ko.toJSON(viewModelTodoItem), false, (resp) => {
            location.href = `/${viewModelTodoItem.username()}/taskedit/${resp}/`;
    
        });*/
	}

	getUserList = (cb: () => void) => {
		//var self = this;
		xhrPost("/api/v1/GetUserList", null, true, (json: any) => {
			this.userList(json);
			if (cb) {
				cb();
			}
		});
	};

	/*self.selectedStatus = ko.observable() // Nothing selected by default*/
}

export class ViewModelBase2 {
	constructor() {
		this.utils = new Utils();
	}
	utils: Utils;
}

export class ViewModelBase extends ViewModelBase2 {
	userId = ko.observable(-1);
	userName = ko.observable("");
	userList = ko.observable([]);
	searchTagUrl = ko.observable("");
	searchTagUrlUsed = ko.observable(false);
	searchShowDone = ko.observable(false);
	searchShowDel = ko.observable(false);
	showInp = ko.observable(true);
	inpCapture = ko.observable(null);
	items = ko.observableArray([]);
	tagItems = ko.observableArray([]);
	tag2TodoItems = ko.observableArray([]);
	addTodoItemName = ko.observable("");
	searchTagItems = ko.observableArray([]);
	searchTerm = ko.observable("");
	itemsModified = ko.observable(false);
	/*filteredItems = ko.pureComputed(function () {
		//var ret =ko.observableArray([]);
		var ret = [];
		if (this.itemsModified()) {
			this.itemsModified(false);
		}
		if (this.searchTerm()) {
			for (var i = 0; i < this.items().length; i++) {
				var item = this.items()[i];
				if (item().name.indexOf(viewModelTodoList.searchTerm()) != -1) {
					ret.push(item);
				}
			}
			//ret([]);
			//ret = [];
		} else {
			if (this.items() && this.items().length > 0) {
				//ret(self.items());
				var index = -1;

				for (var i = 0; i < this.items().length; i++) {
					var item = this.items()[i];
					if (item().id == -1) {
						index = i;
					}
				}

				if (index != -1) {
					var newItem = this.items.splice(index, 1)[0];
					this.items.unshift(newItem);
				}

				ret = this.items();
			}
		}

		return ret;
	});*/

	getUserList = (cb: any) => {
		xhrPost("/api/v1/GetUserList", null, true, (json: any) => {
			this.userList(json);
			if (cb) {
				cb();
			}
		});
	};

	getBaseEntityItemList = function (userId: any, entityType: any, cb: (arg0: any) => void) {
		var apiMethod = "/api/v1/GetTodoItemList";

		var req = {
			userId: userId,
			type: entityType,
			tagBase: "",
			tagSearch: "",
			tagIds: null,
			showDone: false,
			showDel: false,
		};

		xhrPost(apiMethod, ko.toJSON(req), true, (json: any) => {
			if (cb) {
				cb(json);
			}
		});
	};

	getTagByName(name: string) {
		var ret = null;

		for (var i = 0; i < this.tagItems.length; i++) {
			var element = this.tagItems()[i];
			if (element.name == name) {
				ret = element;

				break;
			}
		}

		return ret;
	}
	getTagIds(tagItems: Array<any>) {
		var ret = [];

		for (var i = 0; i < tagItems.length; i++) {
			var element = this.tagItems()[i];
			if (element) {
				ret.push(element.id);
			}
		}

		return ret;
	}
	checkDeltaForFormatting(delta: any) {
		var ret = false;

		for (var i = 0; i < delta.ops.length; i++) {
			var currentChange = delta.ops[i];
			if (currentChange.attributes) {
				ret = true;
			}
		}

		return ret;
	}
	getDueDateVal(data: any) {
		var ret = data.datedue;

		if (data.datePicker) {
			ret = this.utils.getMSFromDateStr(data.datePicker.element.value);
		}

		return ret;
	}
	getTagList(userId: any, cb: any) {
		//viewModelTagList.todoItemId = -1;

		var req = {};
		xhrPost("/api/v1/GetTagList", ko.toJSON(req), true, (json: any) => {
			this.tagItems(json);
			if (this.searchTagUrl()) {
				this.searchTagUrlUsed(true);
				var searchTag = this.getTagByName(this.searchTagUrl());
				this.searchTagItems.push(searchTag);

				this.searchTagUrl("");
			}

			if (cb) {
				cb(this.tagItems);
			}
		});
	}

	getTag2TodoList(userId: any, cb: (arg0: any) => void) {
		//viewModelTagList.todoItemId = -1;
		var req = {};
		xhrPost("/api/v1/GetTag2TodoList", ko.toJSON(req), true, (json: any) => {
			this.tag2TodoItems(json);
			if (cb) {
				cb(this.tag2TodoItems);
			}
		});
	}

	/*
refreshBase =  (cb: (arg0: any) => void) => {
		this.getTagList(this.userId, (tags: any) => {
			this.getTag2TodoList(this.userId, (tag2Todos: any) => {
				if (cb) {
					cb(tag2Todos);
				}
			});
		});

		return true;
	};
*/
	getTitleFromUrl(name: any, cb: (arg0: any) => void) {
		if (cb) {
			cb(name);
		}
	}

	addTodoItem() {
		this.getTitleFromUrl(this.addTodoItemName, (htmlTitle: string | any[]) => {
			var title = this.addTodoItemName;
			var url = "";

			if (htmlTitle && htmlTitle.length > 0) {
				title = htmlTitle;
				url = this.addTodoItemName;
			}

			var item = this.createItem(title, "", "", true, true);
			item.url = url;
			this.items().push(new ko.observable(item));
			this.searchTerm("");
		});
	}

	createItem(name: any, note: string, noteHtml: string, showDetails: boolean, showEdit: boolean) {
		var item = {
			id: "",
			idint: -1,
			name: name,
			notes: note,
			notehtml: noteHtml,
			done: "",
			priority: "",
			url: "",
			showDetails: ko.observable(showDetails),
			showEdit: ko.observable(showEdit),
			showFullNotes: ko.observable(false),
			notesDisplay: ko.observable(""),
			datedueDisplay: ko.observable(""),
			tags: [],
			idusercreated: -1,
			datecreated: 0,
			idusermodified: -1,
			datemodified: 0,
		};

		return item;
	}

	getTodoStatusText(status: TODOSTATUS) {
		var ret = "";

		if (status == TODOSTATUS.INACTIVE) {
			ret = "inactive";
		} else if (status == TODOSTATUS.ARCHIVED) {
			ret = "archived";
		}

		return ret;
	}

	getTodoStatusColor(status: TODOSTATUS, done: any) {
		var ret = "colorItemActive";

		if (done) {
			ret = "colorItemDone";
		} else {
			if (status == TODOSTATUS.INACTIVE) {
				ret = "colorItemInActive";
			} else if (status == TODOSTATUS.ARCHIVED) {
				ret = "colorItemArchived";
			}
		}

		return ret;
	}

	getTaskUrlForType(id: any, type: any) {
		var ret = this.utils.getUrlForType(id, this.userName(), type);
		return ret;
	}

	getUrlForType(id: any, type: any) {
		var ret = this.utils.getUrlForType(id, this.userName(), type);
		return ret;
	}

	getTodoPriorityText(prio: string | number) {
		var ret = prio;

		if (prio <= 0) {
			ret = "";
		} else if (prio == "null") {
			ret = "";
		}

		return ret;
	}

	getCreatedModifiedStr(userIdCreated: any, dateCreated: any, userIdModified: any, dateModified: any, userList: any) {
		var ret = "";
		//var u = getById(viewModelTodoList.userList(), userIdCreated);
		if (userIdCreated) {
			ret = `Created by ${getUserName(userIdCreated, userList)} at ${this.utils.getDateTimeStrFromMS(dateCreated)}`;

			if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
				ret += `, modified by ${getUserName(userIdModified, userList)} at ${this.utils.getDateTimeStrFromMS(dateModified)}`;
			}
		}

		return ret;
	}

	createIdStrForIdInt(idInt: string | number, type: number) {
		return this.utils.createIdStrForIdInt(idInt, type);
	}

	delTodoItem = (data: { id: any }, event: any) => {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiDelTodoItem", ko.toJSON(req), false, (json: any) => {
			//this.getTodoItemList();
		});
	};

	disableTodoItem = (data: { id: any }, event: any) => {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiDisableTodoItem", ko.toJSON(req), false, (json: any) => {
			//this.getTodoItemList();
		});
	};

	archiveTodoItem = (data: { id: any }, event: any) => {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiArchiveTodoItem", ko.toJSON(req), false, (json: any) => {
			//this.getTodoItemList();
		});
	};

	getTodoNotes(data: { notehtml: any; notes: any }) {
		//replace \n with <br>
		var ret = data;
		if (data && data.notehtml) {
			ret = data.notehtml;
		} else if (data && data.notes) {
			ret = data.notes;
		}

		return ret;
	}

	getTodoNotesLine(notes: any, showAll: boolean) {
		var maxLen = 50;
		var ret = "";

		if (showAll) {
			return this.getTodoNotes(notes);
		} else {
			if (notes) {
				var sp = notes.split("\n");
				if (sp.length > 0) {
					ret = sp[0];
					if (ret.length > maxLen) {
						ret = ret.substr(0, maxLen) + "...";
					}
				}
			}
		}

		return ret;
	}
	getTagNames() {
		var ret = [];

		for (var i = 0; i < this.tagItems.length; i++) {
			var element = this.tagItems()[i];
			ret.push(element.name);
		}

		return ret;
	}
}

export class ViewModelTagList {
	items = ko.observableArray([]);
	addTagItemName = ko.observable("");

	delTagItem = (data: { id: any }, event: any) => {
		var req = {
			tagId: data.id,
		};

		xhrPost("/todo/apiDelTag", ko.toJSON(req), false, (json: any) => {
			//this.getTagList();
		});
	};
}

export function xhrPost(url: string, json: null, parseJson: boolean, cb: any) {
	fetch(url, {
		method: "post",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: json,
	})
		.then((r) => {
			if (parseJson) {
				r.json().then((resp) => {
					var i = resp;
					if (cb) {
						cb(resp);
					}
				});
			} else {
				r.text().then((resp) => {
					console.log(resp);
					if (cb) {
						cb(resp);
					}
				});
			}
		})
		.catch(function (error) {
			console.error(error);
		});
}

export function checkAndFixUrl(url: string) {
	var ret = url;
	if (typeof url == "function") {
		//ret = ret();
	}

	if (ret) {
		if (ret.indexOf("http") == -1) {
			ret = "http://" + url;
			if (typeof url == "function") {
				//ret = url(ret);
			}
		}
	}

	return ret;
}

export function shortenString(str: string, maxLen: number) {
	var ret: string = str ?? "";

	if (typeof str == "function") {
		//ret = ret();
	}

	if (ret) {
		if (ret.indexOf("https:")) {
			ret = ret.substr("https://".length + 1);
		} else if (ret.indexOf("http:")) {
			ret = ret.substr("http://".length + 1);
		}

		if (str.length > maxLen) {
			var half = maxLen / 2;
			var part1 = ret.substr(0, half);
			var part2 = ret.substr(ret.length + 3 - half, half - 3);
			ret = `${part1}...${part2}`;
		}
	}

	return ret;
}

export function getIndexById(arr: any[], id: any) {
	var ret = -1;

	var myArr: any[] = arr;
	if (typeof arr == "function") {
		//myArr = arr(); //is this a knockout observable?
	}

	for (var i = 0; i < myArr.length; i++) {
		//var a = myArr as Array<any>;
		var element = myArr[i];
		if (element.id == id) {
			ret = i;

			break;
		}
	}

	return ret;
}

function getById(arr: any[], id: any) {
	var ret = null;

	var index = getIndexById(arr, id);
	if (index != -1) {
		ret = arr[index];
	}

	return ret;
}

function createItemBase(data: { idnote: any; iduser: any }, type: number, event: any) {
	var req = {
		idnote: data.idnote,
		iduser: data.iduser,
		type: type,
	};

	xhrPost("/todo/apiJournalCreateItem", ko.toJSON(req), true, (json: any) => {
		/*if (cb) {
            cb(json);
        }*/
	});

	return null;
}

function createKnowledgebaseItem(data: any, event: any) {
	return createItemBase(data, 2, event);
}

function createTaskItem(data: any, event: any) {
	return createItemBase(data, 4, event);
}

function createTodoItem(data: any, event: any) {
	return createItemBase(data, 0, event);
}

function addUpdateNote(
	data: {
		idnote: any;
		idfk: any;
		iduser: any; //,
		status: any;
		notes: string;
		notehtml: string;
	},
	cb: { (resp: any): void; (): void }
) {
	xhrPost("/api/v1/NoteUpd", ko.toJSON(data), false, (resp: any) => {
		if (cb) {
			cb();
		}
	});
}

export function getTaskGroupItems(data: any, cb: (arg0: any) => void) {
	xhrPost("/api/v1/GetTaskGroupList", ko.toJSON(data), true, (resp: any) => {
		if (cb) {
			cb(resp);
		}
	});
}

/*
function createTodoItem(data, event) {
    var req = {
        idnote: data.idnote,
        iduser: data.iduser
    };

    xhrPost('/todo/apiJournalCreateTodoItem', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    

    return null;
}
*/

function getUserName(userId: any, userList?: any) {
	var ret = "";

	if (userList) {
		var user = getById(userList, userId) as any;
		if (user && user.id) {
			ret = user.name;
		}
	}

	return ret;
}
