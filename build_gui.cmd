copy .\public\src\muidRemove.png .\public\dist\muidRemove.png
copy .\common\base\public\base.css .\public\dist\base\base.css
copy .\common\base\public\base.css .\public\dist\base\base.css
copy .\modules\tasks\public\task.css .\public\dist\tasks\task.css
copy .\modules\todo\public\todo.css .\public\dist\todo\todo.css

deno bundle -c .\public\src\tsconfig.json --unstable .\modules\tasks\public\tasklist.ts .\public\dist\tasks\tasklist.js
deno bundle -c .\public\src\tsconfig.json --unstable .\modules\tasks\public\taskedit.ts .\public\dist\tasks\taskedit.js
deno bundle -c .\public\src\tsconfig.json --unstable .\modules\journal\public\journallist.ts .\public\dist\journal\journallist.js
deno bundle -c .\public\src\tsconfig.json --unstable .\modules\kb\public\kblist.ts .\public\dist\kb\kblist.js
deno bundle -c .\public\src\tsconfig.json --unstable .\modules\todo\public\todolist.ts .\public\dist\todo\todolist.js

sed -i "s/var ko =/var koOld =/g" .\public\dist\tasks\tasklist.js
sed -i "s/var ko1 =/var koOld1 =/g" .\public\dist\tasks\tasklist.js
sed -i "s/ko1./ko./g" .\public\dist\tasks\tasklist.js
sed -i "s/var document =/var documentOld =/g" .\public\dist\tasks\tasklist.js

sed -i "s/var ko =/var koOld =/g" .\public\dist\tasks\taskedit.js
sed -i "s/var ko1 =/var koOld1 =/g" .\public\dist\tasks\taskedit.js
sed -i "s/ko1./ko./g" .\public\dist\tasks\taskedit.js
sed -i "s/var document =/var documentOld =/g" .\public\dist\tasks\taskedit.js
sed -i "s/Quill =/QuillOld =/g" .\public\dist\tasks\taskedit.js

sed -i "s/var ko =/var koOld =/g" .\public\dist\journal\journallist.js
sed -i "s/var ko1 =/var koOld1 =/g" .\public\dist\journal\journallist.js
sed -i "s/ko1./ko./g" .\public\dist\journal\journallist.js
sed -i "s/var document =/var documentOld =/g" .\public\dist\journal\journallist.js
sed -i "s/Quill =/QuillOld =/g" .\public\dist\journal\journallist.js
sed -i "s/this.fakeTest/\/\/this.fakeTest/g" .\public\dist\journal\journallist.js


sed -i "s/var ko =/var koOld =/g" .\public\dist\kb\kblist.js
sed -i "s/var ko1 =/var koOld1 =/g" .\public\dist\kb\kblist.js
sed -i "s/ko1./ko./g" .\public\dist\kb\kblist.js
sed -i "s/var document =/var documentOld =/g" .\public\dist\kb\kblist.js
sed -i "s/Quill =/QuillOld =/g" .\public\dist\kb\kblist.js
sed -i "s/this.fakeTest/\/\/this.fakeTest/g" .\public\dist\kb\kblist.js

sed -i "s/var ko =/var koOld =/g" .\public\dist\todo\todolist.js
sed -i "s/var ko1 =/var koOld1 =/g" .\public\dist\todo\todolist.js
sed -i "s/ko1./ko./g" .\public\dist\todo\todolist.js
sed -i "s/var document =/var documentOld =/g" .\public\dist\todo\todolist.js
sed -i "s/Quill =/QuillOld =/g" .\public\dist\todo\todolist.js
sed -i "s/this.fakeTest/\/\/this.fakeTest/g" .\public\dist\todo\todolist.js

