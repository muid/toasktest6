import { Router, Context, RouterContext } from "../../deps/oak.ts";
import { renderFile } from "../../deps/pug.ts";
import { DbController, SQLTYPE } from "../../common/db/DbController.ts";
import { UserController } from "../../common/users/UserController.ts";
import { TagController } from "../../common/tags/TagController.ts";
import { TodoController } from "./TodoController.ts";
import { BaseController } from "../../common/base/BaseController.ts";
import { MyContext } from "../../common/MyContext.ts";
//import {} from "../../common/"
import { ViewModelBase, ViewModelEditBase, ViewModelNotesBase, ViewModelTagList, xhrPost, getTaskGroupItems, TODOTYPE, TODOSTATUS, TODONOTESTATUS } from "../../common/base/public/base.ts";
import { Utils } from "../../public/src/shared/Utils.ts";

var userController: UserController;
var tagController: TagController;
var dbController: DbController;
var baseController: BaseController;
var todoController: TodoController;
var utils: Utils;

export function use(path: string, app: Router, db: DbController, myContext: MyContext) {
	dbController = db;
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	baseController = new BaseController(db, myContext);
	todoController = new TodoController(db, myContext);

	return app
		.get("/:user/todo", (ctx: RouterContext) => {
			/*if (!req.session.loggedIn) {
			return res.redirect(`./`);
		}*/

			//updateIdIntForTable("todo",TODOTYPE.TODO);

			//updateIdForTable("todo", "uid");
			//updateUserIdForTable("CoinInvestmentGroup", "uiduser");
			//updateUserIdForTable("CoinInvestment", "uiduser");
			//updateUserIdForTable("Todo", "idusercreated", "uidusercreated");
			//return ;

			//debug
			//kbSyncItems();

			//var userName = req.params.user;
			var userName = ctx.params.user;
			if (userName) {
				userController.userGetByName(userName, (user: any) => {
					if (user) {
						//updateIdIntForTable("todo", 0);
						//res.render("todolist", { userid: user.id, username: user.name });
						ctx.response.body = renderFile("modules/todo/views/todolist.pug", { userid: user.id, username: user.name });
					}
				});
			}
		})
		.get("/:user/todoedit/:todoId", (ctx: RouterContext) => {
			var todoIdStr: string = "";
			if (ctx.params.todoId) {
				todoIdStr = ctx.params.todoId;
				//var todoId = parseInt(req.params.todoId);

				//if (todoId)
				{
					tagController.tagGetItems((tags: Array<any>) => {
						tagController.tagGetItemsForTodoItem(todoIdStr, (itemTags: Array<any>) => {
							baseController.todoGetItem(todoIdStr, (todoItem: any) => {
								//res.render('todoedit', { todoitem: todoItem, tags: tags, itemTags: itemTags, user: user.n  })
								//res.render("todoedit", { todoitem: todoItem, tags: tags, itemTags: itemTags });
								ctx.response.body = renderFile("modules/todo/views/todoedit.pug", { todoitem: todoItem, tags: tags, itemTags: itemTags });
							});
						});
					});
				}
				//else {
				//}
				//getList(0, 0, res);
			}
		});
}
