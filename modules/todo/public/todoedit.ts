var viewModelTodoItem = {};
var viewModelTag = {};

var createViewModelTodoItem = function (id, name, url, priority, done, notes) {
    var self = this;
  
    self.id = ko.observable(id);
    self.name = ko.observable(name);
    self.url = ko.observable(url);
    self.priority = ko.observable(priority);
    self.done = ko.observable(done);
    self.notes = ko.observable(notes);
};

var createViewModelTagRelation = function (tagId, idTodoItem) {
    var self = this;
  
    self.tagId = ko.observable(tagId);
    self.todoItemId = ko.observable(idTodoItem);
    self.tagName = ko.observable("");
}

function saveTodoItem() {
    xhrPost('/todo/apiUpd', ko.toJSON(viewModelTodoItem));
}

function addTagRelation() {
    xhrPost('/todo/apiAddTagRelation', ko.toJSON(viewModelTag));
}

function delTagRelation(tagId) {
    viewModelTag.tagId = tagId;

    xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTag));
}

