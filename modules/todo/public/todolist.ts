import { ViewModelBase, ViewModelBase2, ViewModelTagList, xhrPost, TODOSTATUS } from "../../../common/base/public/base.ts";

var ko: any = {};
var document: any = {};
var Quill: any = {};
var autoComplete: any;
var viewModelTodoList: ViewModelTodoList;

//import utils from "../../../public/shared/utils";
//import * as base from "../../../common/base/public/base"

//var viewModelTodoList = {};
var viewModelTagList = new ViewModelTagList();
var viewModelDueList = {};

class ViewModelTodoList extends ViewModelBase {
	self = this;

	userId = ko.observable(-1);
	userList = ko.observable([]);
	searchTagUrl = ko.observable("");
	searchTagUrlUsed = ko.observable(false);
	searchShowDone = ko.observable(false);
	searchShowDel = ko.observable(false);
	showInp = ko.observable(true);
	inpCapture = ko.observable(null);

	items = ko.observableArray([]);
	tagItems = ko.observableArray([]);
	tag2TodoItems = ko.observableArray([]);
	addTodoItemName = ko.observable("");
	searchTagItems = ko.observableArray([]);
	searchTerm = ko.observable("");
	itemsModified = ko.observable(false);
	filteredItems = ko.pureComputed(() => {
		//var ret =ko.observableArray([]);
		var ret = [];
		if (this.itemsModified()) {
			this.itemsModified(false);
		}
		if (this.searchTerm()) {
			for (var i = 0; i < this.items().length; i++) {
				var item = this.items()[i];
				if (item().name.indexOf(this.searchTerm()) != -1) {
					ret.push(item);
				}
			}
			//ret([]);
			//ret = [];
		} else {
			if (this.items() && this.items().length > 0) {
				//ret(this.items());
				var index = -1;

				for (var i = 0; i < this.items().length; i++) {
					var item = this.items()[i];
					if (item().id == -1) {
						index = i;
					}
				}

				if (index != -1) {
					var newItem = this.items.splice(index, 1)[0];
					this.items.unshift(newItem);
				}

				ret = this.items();
			}
		}

		return ret;
	}, self);

	refresh(data: any, event: any) {
		this.getTodoItemList();
		return true;
	}

	getTagsForTodoItemId(id: string) {
		var ret: Array<any> = [];

		//if (id != -1)
		{
			viewModelTodoList.tag2TodoItems().forEach((element: any) => {
				if (element.idtodo == id) {
					var tag = this.utils.getById(viewModelTodoList.tagItems(), element.idtag, "");
					ret.push(tag);
				}
			});
		}

		return ret;
	}

	getTodoItemList() {
		var apiMethod = "/api/v1/GetTodoItemList";

		var req = {
			userId: viewModelTodoList.userId,
			tagBase: "",
			tagSearch: "",
			tagIds: [undefined],
			showDone: viewModelTodoList.searchShowDone,
			showDel: viewModelTodoList.searchShowDel,
		};

		if (viewModelTodoList.searchTagItems.length > 0) {
			req.tagIds = this.getTagIds(viewModelTodoList.searchTagItems);
			apiMethod = "/api/v1/GetTodoItemListByTags";
		}

		xhrPost(apiMethod, ko.toJSON(req), true, (json: any) => {
			for (var i = 0; i < json.length; i++) {
				json[i].showDetails = ko.observable(false);
				json[i].showEdit = ko.observable(false);
				json[i].showFullNotes = ko.observable(false);
				json[i].notesDisplay = ko.observable(this.getTodoNotesLine(json[i].notes, false));
				json[i].tags = this.getTagsForTodoItemId(json[i].id);
				json[i].datedueDisplay = this.utils.getDateStrFromMS(json[i].datedue);
				json[i].datedue = json[i].datedue;
				json[i].datePicker = null;

				json[i] = ko.observable(json[i]);
			}

			viewModelTodoList.items(json);
			//viewModelDueList.items(json);
		});
	}

	/*delTodoItem(data: any, event: any) {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiDelTodoItem", ko.toJSON(req), false, (json: any) => {
			this.getTodoItemList();
		});
	}

	disableTodoItem(data: any, event: any) {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiDisableTodoItem", ko.toJSON(req), false, (json: any) => {
			getTodoItemList();
		});
	}

	archiveTodoItem(data: any, event: any) {
		var req = {
			id: data.id,
		};

		xhrPost("/todo/apiArchiveTodoItem", ko.toJSON(req), false, (json: any) => {
			getTodoItemList();
		});
	}
*/
	removeSearchTag(data: any, event: any) {
		if (!(this.searchTagUrlUsed() && this.searchTagItems.length == 1)) {
			var tagIndex = this.utils.getIndexById(this.searchTagItems, data.id, "");
			this.searchTagItems.splice(tagIndex, 1);
			this.getTodoItemList();
		}
	}

	removeTagRelation(data: any, event: any) {
		//viewModelTag.tagId = data;

		xhrPost("/todo/apiDelTagRelation", ko.toJSON(viewModelTagList), false, undefined);
	}

	saveTodoItem(data: any, event: any) {
		var req = {
			id: data.id,
			userid: this.userId,
			idnote: data.idnote,
			type: 0,
			name: data.name,
			notes: data.notes,
			notehtml: data.notehtml,
			done: data.done,
			priority: data.priority,
			status: data.status,
			url: data.url,
			duedate: this.getDueDateVal(data),
		};

		xhrPost("/todo/apiUpd", ko.toJSON(req), false, (json: any) => {
			this.toggleEdit(data);
			this.addTodoItemName("");
			this.getTodoItemList();
		});

		return true;
	}

	toggle(item: any) {
		if (item.showDetails()) {
			item.showDetails(false);
		} else {
			item.showDetails(true);
		}
		/* if (this.showInp()) {
            this.showInp(false);
        } else {
            this.showInp(true);
        }*/
	}

	toggleEdit(item: any) {
		if (item.showEdit()) {
			item.showEdit(false);
		} else {
			item.showEdit(true);
		}
	}

	toggleShowFullNotes(item: any) {
		if (item.showFullNotes()) {
			item.showFullNotes(false);
			item.notesDisplay(this.getTodoNotesLine(item.notes, false));
		} else {
			item.showFullNotes(true);
			item.notesDisplay(this.getTodoNotesLine(item.notes, true));
		}
	}

	addTagItemName = ko.observable("");

	delTagItem(data: any, event: any) {
		var req = {
			tagId: data.id,
		};

		xhrPost("/todo/apiDelTag", ko.toJSON(req), false, (json: any) => {
			this.getTagList(undefined, undefined);
		});
	}
	addTodoItemNote() {
		var note = "";
		var noteHtml = "";
		var delta = viewModelTodoList.inpCapture().getContents();
		note = viewModelTodoList.inpCapture().getText();
		if (this.checkDeltaForFormatting(delta)) {
			noteHtml = viewModelTodoList.inpCapture().getHtml();
			//note = viewModelTodoList.inpCapture().getHtml();
		} else {
			note = viewModelTodoList.inpCapture().getText();
		}

		var item = this.createItem("", note, noteHtml, true, true);
		viewModelTodoList.items().push(new ko.observable(item));
		viewModelTodoList.itemsModified(true);
		//viewModelTodoList.searchTerm("");
	}
	fakeTest() {
		init("");
		this.getTodoStatusColor(TODOSTATUS.ACTIVE, false);
	}
}
class ViewModelDueList extends ViewModelBase2 {
	self = this;

	items = ko.observableArray([]);
	filteredDueTodayItems = ko.pureComputed(() => {
		var ret = [];

		if (this.items() && this.items().length > 0) {
			var dateTodayStr = this.utils.getDateStrFromMS(new Date().getTime());

			for (var i = 0; i < this.items().length; i++) {
				var item = this.items()[i];
				if (item().id == -1) {
					var newItem = this.items.splice(i, 1)[0];
					this.items.unshift(newItem);
					i = 0;
					continue;
				}

				if (item().datedue && this.utils.getDateStrFromMS(item().datedue) == dateTodayStr) {
					ret.push(item);
				}
			}
		}

		return ret;
	}, self);

	filteredDueSoonItems = ko.pureComputed(() => {
		var ret = [];

		var dateNow = new Date().getTime();

		if (this.items() && this.items().length > 0) {
			for (var i = 0; i < this.items().length; i++) {
				var item = this.items()[i];
				if (item().id == -1) {
					var newItem = this.items.splice(i, 1)[0];
					this.items.unshift(newItem);
					i = 0;
					continue;
				}

				if (item().datedue && item().datedue > dateNow) {
					ret.push(item);
				}
			}
		}

		return ret;
	}, self);

	filteredPastDueItems = ko.pureComputed(() => {
		var ret = [];

		var dateNow = new Date().getTime();
		var dateTodayStr = this.utils.getDateStrFromMS(dateNow);

		if (this.items() && this.items().length > 0) {
			for (var i = 0; i < this.items().length; i++) {
				var item = this.items()[i];
				if (item().id == -1) {
					var newItem = this.items.splice(i, 1)[0];
					this.items.unshift(newItem);
					i = 0;
					continue;
				}

				if (item().datedue && item().datedue < dateNow) {
					if (this.utils.getDateStrFromMS(item().datedue) != dateTodayStr) ret.push(item);
				}
			}
		}

		return ret;
	}, self);
}

function init(userId: string) {
	viewModelTodoList = new ViewModelTodoList();
	viewModelDueList = new ViewModelDueList();
	viewModelTagList = new ViewModelTagList();
	ko.applyBindings(viewModelTodoList, document.getElementById("tdTodoListId"));
	ko.applyBindings(viewModelDueList, document.getElementById("tdDueListId"));
	ko.applyBindings(viewModelTagList, document.getElementById("tdTagListId"));

	viewModelTodoList.getUserList(undefined);

	if (userId && viewModelTodoList.userId() == -1) {
		viewModelTodoList.userId(userId);
	}

	viewModelTodoList.refresh(undefined, undefined);

	Quill.prototype.getHtml = function () {
		return this.container.querySelector(".ql-editor").innerHTML;
	};
	viewModelTodoList.inpCapture(
		new Quill("#bubble-container", {
			placeholder: "Enter a note for the new todo item...",
			theme: "snow",
		})
	);

	var ac = autoComplete({
		selector: 'input[name="q"]',
		minChars: 0,
		onSelect: function (e: any, term: any, item: any) {
			var k = 2;
			var tag = viewModelTodoList.getTagByName(term);
			viewModelTodoList.searchTagItems.push(tag);
			document.getElementById("inpAddTodoItem").value = "";
			viewModelTodoList.getTodoItemList();
		},
		onSuggestNone: function (term: string) {
			viewModelTodoList.searchTerm(term);
			var t = 0;
		},
		source: function (term: string, suggest: any) {
			term = term.toLowerCase();
			var choices = viewModelTodoList.getTagNames(); // ['ActionScript', 'AppleScript', 'Asp'];
			var matches = [];
			for (var i = 0; i < choices.length; i++) {
				var item = choices[i].toLowerCase();

				//var index  = ~choices[i].toLowerCase().indexOf(term);
				var index = item.indexOf(term);
				//if (~choices[i].toLowerCase().indexOf(term)) {
				if (index != -1) {
					matches.push(choices[i]);
				} else {
					if (term == " ") {
						matches.push(choices[i]);
					}
				}
			}
			suggest(matches, term);
		},
	});
}

function clearInput(id: string) {
	document.getElementById(id).value = "";
}
/*
this.createItem = function (name, note, noteHtml, showDetails, showEdit) {
	var item = {
		id: -1,
		name: name,
		notes: note,
		notehtml: noteHtml,
		done: "",
		priority: "",
		url: "",
		showDetails: ko.observable(showDetails),
		showEdit: ko.observable(showEdit),
		showFullNotes: ko.observable(false),
		notesDisplay: ko.observable(""),
		datedueDisplay: ko.observable(""),
		tags: [],
		idusercreated: -1,
		datecreated: 0,
		idusermodified: -1,
		datemodified: 0,
	};

	return item;
};
*/
/*
function addTodoItem() {
	getTitleFromUrl(viewModelTodoList.addTodoItemName, (htmlTitle) => {
		var title = viewModelTodoList.addTodoItemName;
		var url = "";

		if (htmlTitle && htmlTitle.length > 0) {
			title = htmlTitle;
			url = viewModelTodoList.addTodoItemName;
		}

		var item = createItem(title, "", "", true, true);
		item.url = url;
		viewModelTodoList.items().push(new ko.observable(item));
		viewModelTodoList.searchTerm("");
	});
}



function setSearchTerm(data) {
	viewModelTodoList.addTodoItemName(data.name);
	viewModelTodoList.searchTerm(data.name);
	setTimeout(() => {
		data.showDetails(true);
	}, 100);
}

function addTodoItemold() {
	var req = {
		name: viewModelTodoList.addTodoItemName,
	};

	xhrPost("/todo/apiUpd", ko.toJSON(req), false, (json) => {
		viewModelTodoList.addTodoItemName("");
		//clearInput('inpAddTodoItem');
		//getTodoItemList();
		refresh();
	});
}
 

function addTagRelation(data) {
	var req = {
		tagId: 0,
		tagName: viewModelTodoList.addTagItemName,
		todoItemId: data.id,
	};

	xhrPost("/todo/apiAddTagRelation", ko.toJSON(req), false, (json) => {
		viewModelTodoList.toggleEdit(data);
		viewModelTodoList.addTagItemName("");
		init();
		//getTagList();
	});
}

function refresh() {
	var userId = viewModelTodoList.userId();

	getTagList(userId, (tags) => {
		getTag2TodoList(userId, (tag2Todos) => {
			getTodoItemList(userId, (tag2Todos) => {});
		});
	});
}

*/
/*
function getTagIndexById(id) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.id == id) {
            ret = i;
             
            break;
        }
    }

    return ret;
}
function getTagById(id) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.id == id) {
            ret = element;
             
            break;
        }
    }

    return ret;
}
*/
/*
function getTagNames() {
	var ret = [];

	for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
		var element = viewModelTodoList.tagItems()[i];
		ret.push(element.name);
	}

	return ret;
}

function getTagIds(tagItems) {
	var ret = [];

	for (var i = 0; i < tagItems.length; i++) {
		var element = tagItems()[i];
		if (element) {
			ret.push(element.id);
		}
	}

	return ret;
}

function getTagByName(name) {
	var ret = null;

	for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
		var element = viewModelTodoList.tagItems()[i];
		if (element.name == name) {
			ret = element;

			break;
		}
	}

	return ret;
}


function getTagList(userId, cb) {
	viewModelTagList.todoItemId = -1;

	xhrPost("/api/v1/GetTagList", ko.toJSON(viewModelTagList), true, (json) => {
		viewModelTodoList.tagItems(json);
		if (viewModelTodoList.searchTagUrl()) {
			viewModelTodoList.searchTagUrlUsed(true);
			var searchTag = getTagByName(viewModelTodoList.searchTagUrl());
			viewModelTodoList.searchTagItems.push(searchTag);

			viewModelTodoList.searchTagUrl("");
		}

		if (cb) {
			cb(viewModelTodoList.tagItems);
		}
	});
}

function getUserList(cb) {
	xhrPost("/api/v1/GetUserList", null, true, (json) => {
		viewModelTodoList.userList(json);
		if (cb) {
			cb(viewModelTodoList.tagItems);
		}
	});
}

function getTag2TodoList(userId, cb) {
	viewModelTagList.todoItemId = -1;

	xhrPost("/todo/apiGetTag2TodoList", ko.toJSON(viewModelTagList), true, (json) => {
		viewModelTodoList.tag2TodoItems(json);
		if (cb) {
			cb(viewModelTodoList.tag2TodoItems);
		}
	});
}

function getTitleFromUrl(url, cb) {
	var myUrl = url;
	if (typeof url == "function") {
		myUrl = url();
	}

	if (!utils.isUrl(myUrl)) {
		cb("");
		return;
	}

	var req = {
		url: url,
	};

	xhrPost("/todo/apiGetTitleFromUrl", ko.toJSON(req), true, (json) => {
		if (cb) {
			cb(json);
		}
	});
}

var TODOSTATUS = {
	ACTIVE: 0,
	INACTIVE: 1,
	ARCHIVED: 2,
};

function getTodoStatusText(status) {
	var ret = "";

	if (status == TODOSTATUS.INACTIVE) {
		ret = "inactive";
	} else if (status == TODOSTATUS.ARCHIVED) {
		ret = "archived";
	}

	return ret;
}

function getTodoStatusColor(status, done) {
	var ret = "colorItemActive";

	if (done) {
		ret = "colorItemDone";
	} else {
		if (status == TODOSTATUS.INACTIVE) {
			ret = "colorItemInActive";
		} else if (status == TODOSTATUS.ARCHIVED) {
			ret = "colorItemArchived";
		}
	}

	return ret;
}

function getTodoPriorityText(prio) {
	var ret = prio;

	if (prio <= 0) {
		ret = "";
	} else if (prio == "null") {
		ret = "";
	}

	return ret;
}

function getTodoNotesLine(notes, showAll) {
	var maxLen = 50;
	var ret = "";

	if (showAll) {
		return getTodoNotes(notes);
	} else {
		if (notes) {
			var sp = notes.split("\n");
			if (sp.length > 0) {
				ret = sp[0];
				if (ret.length > maxLen) {
					ret = ret.substr(0, maxLen) + "...";
				}
			}
		}
	}

	return ret;
}

function getTodoNotes(data) {
	//replace \n with <br>
	var ret = data;
	if (data && data.notehtml) {
		ret = data.notehtml;
	} else if (data && data.notes) {
		ret = data.notes;
	}

	return ret;
}

function getTodoEditUrl(id) {
	return "/todo/edit/" + id;
}

 

function getTodoSearchUrl(tag) {
	//var tag = item.name;
	return "/todo/" + tag;
}

function getUserName(userId) {
	var ret = "";

	var user = getById(viewModelTodoList.userList(), userId);
	if (user && user.id) {
		ret = user.name;
	}

	return ret;
}
function getCreatedModifiedStr(userIdCreated, dateCreated, userIdModified, dateModified) {
	var ret = "";
	var u = getById(viewModelTodoList.userList(), userIdCreated);
	if (userIdCreated) {
		ret = `Created by ${getUserName(userIdCreated)} at ${utils.getDateTimeStrFromMS(dateCreated)}`;

		if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
			ret += `, modified by ${getUserName(userIdModified)} at ${utils.getDateTimeStrFromMS(dateModified)}`;
		}
	}

	return ret;
}

function showDatePicker(data, ev) {
	if (!data.datePicker) {
		data.datePicker = setDatePicker(data, ev.target);
	}

	if (data.datePicker) {
		data.datePicker.show();
	}
}



function setDatePicker(data, elem) {
	return new Datepicker(elem, {
		// ...options
		format: "dd.mm.yyyy",
		weekStart: 1,
		//showOnClick: true,
		todayBtn: true,
		todayBtnMode: 1,
		todayHighlight: true,
		//autohide: true,
		changeDate: (d) => {
			var i = 0;
		},
	});
}
*/
