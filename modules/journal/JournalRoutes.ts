/*
import { Application, Request, Response } from "express";
import sqlite from "sqlite3";
import { DbController, SQLTYPE } from "../../common/db/DbController";
import { UserController } from "../../common/users/UserController";
import { TagController } from "../../common/tags/TagController";
import { BaseController } from "../../common/base/BaseController";
import { Utils } from "../../public/shared/Utils";
import { TODOTYPE, TODOSTATUS, TODONOTESTATUS } from "../../public/shared/Types";
import { MyContext } from "../../common/MyContext";
*/
import { Router, Context, RouterContext } from "../../deps/oak.ts";
import { renderFile } from "../../deps/pug.ts";
import { DbController, SQLTYPE } from "../../common/db/DbController.ts";
import { UserController } from "../../common/users/UserController.ts";
import { TagController } from "../../common/tags/TagController.ts";
import { JournalController } from "./JournalController.ts";
import { BaseController } from "../../common/base/BaseController.ts";
import { MyContext } from "../../common/MyContext.ts";
//import {} from "../../common/"
import { ViewModelBase, ViewModelEditBase, ViewModelNotesBase, ViewModelTagList, xhrPost, getTaskGroupItems, TODOTYPE, TODOSTATUS, TODONOTESTATUS } from "../../common/base/public/base.ts";
import { Utils } from "../../public/src/shared/Utils.ts";

var userController: UserController;
var tagController: TagController;
var dbController: DbController;
var baseController: BaseController;
var utils: Utils;

export function use(path: string, app: Router, db: DbController, myContext: MyContext) {
	//export function JournalRoute(app: Application, db: sqlite.Database, myContext: MyContext) {
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	dbController = db;
	baseController = new BaseController(db, myContext);
	utils = new Utils();

	return app
		.get("/:user/journal", (ctx: RouterContext) => {
			/*if (!req.session.loggedIn) {
			return res.redirect(`./`);
		}*/

			var userName = ctx.params.user;

			if (userName) {
				userController.userGetByName(userName, (user: any) => {
					if (user && user.id != -1) {
						//updateIdIntForTable("todo", 1);
						//res.render("journallist", { userid: user.id, username: user.name });
						ctx.response.body = renderFile("modules/journal/views/journallist.pug", { userid: user.id, username: user.name });
					}
				});
			}
		})
		.get("/:user/unsorted", (ctx: RouterContext) => {
			/*if (!req.session.loggedIn) {
			return res.redirect(`./`);
		}*/

			var userName = ctx.params.user;

			if (userName) {
				userController.userGetByName(userName, (user: any) => {
					if (user && user.id != -1) {
						//res.render("unsortedlist", { userid: user.id, username: user.name });
						ctx.response.body = renderFile("modules/journal/views/unsortedlist.pug", { userid: user.id, username: user.name });
					}
				});
			}
		})
		.post("/api/v1/JournalUpd", async function (ctx: RouterContext) {
			//var item = req.body;
			const body = ctx.request.body();
			const item = await body.value;

			journalGetItem(item.id, item.duedate, (itemFound: any) => {
				if (itemFound) {
					if (item.onlyUpdateNote) {
						if (item.type == TODOTYPE.UNSORTED) {
							baseController.noteUpdateTodoId(item.idnote, item.idjournal, TODOTYPE.JOURNAL, () => {
								ctx.response.status = 200;
							});
						} else if (item.status == TODONOTESTATUS.DELETED) {
							baseController.todoUpdateNoteStatus(item.idnote, item.status, () => {
								ctx.response.status = 200;
							});
						} else {
							baseController.noteUpdateText(item.idnote, item.notes, item.notehtml, item.userid, () => {
								ctx.response.status = 200;
							});
						}
					} else {
						baseController.noteAddOrUpdate(item.idnote, itemFound.id, item.userid, 1, 0, item.notes, item.notehtml, () => {
							ctx.response.status = 200;
						});
					}
				} else {
					baseController.todoAddOrUpdate(item.id, item.userid, "", "", item.type, item.name, item.notes, item.url, item.done, item.priority, item.status, item.duedate, (lastId: string) => {
						if (lastId && item.notes && item.notes.length > 0) {
							baseController.noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, () => {
								ctx.response.status = 200;
							});
						} else {
							ctx.response.status = 200;
						}
					});
				}
			});
		})
		.post("/todo/apiJournalCreateItem", async function (ctx: RouterContext) {
			const body = ctx.request.body();
			const params = await body.value;
			var userId = params.iduser;
			var noteId = params.idnote;
			var itemType = params.type;

			var item = baseController.noteGetItem(noteId); //, (item: any) => {
			var name = baseController.getTodoNotes(item, true);
			if (name) {
				name = name.substr(0, 50);
			}

			baseController.todoAddOrUpdate("", userId, noteId, "", itemType, name, "", "", false, 0, 0, 0, (todoId: string) => {
				baseController.noteUpdateTodoId(noteId, todoId, itemType);

				ctx.response.status = 200;
			});
			//			});
		})
		.post("/api/v1/GetJournalItemList", async function (ctx: RouterContext) {
			var ret = new Array<any>();
			var json = "{}";
			const body = ctx.request.body();
			const params = await body.value;

			var userId = params.userId;
			var showDone = params.showDone;
			var type = params.type;
			var typeOrg = params.type;

			if (type == TODOTYPE.UNSORTED) {
				type = TODOTYPE.JOURNAL;
			}
			//var searchShowDel = params.showDel;

			var statusSet = 0;
			if (params.showDel) {
				statusSet = TODOSTATUS.INACTIVE;
			}

			var tagBase = params.tagBase;
			var tagSearch = params.tagSearch;
			var tags: any; // = null;

			var journalItems: any = await baseController.todoGetItems(userId, type, [], tags, tagBase, tagSearch, showDone, statusSet); //, async (journalItems: any, tags: any) => {
			//async function asyncRunner() {
			try {
				//updateIdIntForTable("note", -1);
				for (var i = 0; i < journalItems.length; i++) {
					//if (!rows[i].uid)
					var notes = await baseController.noteGetItemsForP(journalItems[i].id); //, (notes: []) => {
					for (var j = 0; j < notes.length; j++) {
						var note: any = notes[j];
						if (typeOrg == TODOTYPE.UNSORTED) {
							if (note.idtodo || note.idjournal || note.idkb || note.idtask) {
								continue;
							}
						}

						if (note.status == TODONOTESTATUS.DELETED) {
							continue;
						}

						var itemBase = utils.cloneObject(journalItems[i]);
						itemBase["mainEntry"] = false;
						if (j == 0) {
							itemBase.mainEntry = true;
						}

						itemBase.idnote = note.id;
						itemBase.idkb = note.idkb;
						itemBase.idtodo = note.idtodo;
						itemBase.idtask = note.idtask;
						itemBase.idjournal = note.idjournal;

						itemBase.notes = note.note;

						if ((note.notehtml && note.notehtml.length == 0) || !note.notehtml) {
							itemBase.notehtml = note.note;
						} else {
							itemBase.notehtml = note.notehtml;
						}

						itemBase.datemodified = note.datemodified;
						itemBase.idusermodified = note.idusermodified;
						itemBase.datecreated = note.datecreated;
						itemBase.idusercreated = note.idusercreated;

						ret.push(itemBase);
					}
					//	});

					for (var k = 0; k < ret.length; k++) {
						var item = ret[k];

						if (!item.idintkb) {
							item.idintkb = -1;

							if (item.idkb) {
								await baseController.todoGetItemP(item.idkb, (todoItem: any) => {
									item.idintkb = todoItem.idint;
								});
							}
						}

						if (!item.idinttodo) {
							item.idinttodo = -1;

							if (item.idtodo) {
								await baseController.todoGetItemP(item.idtodo, (todoItem: any) => {
									item.idinttodo = todoItem.idint;
								});
							}
						}

						if (!item.idinttask) {
							item.idinttask = -1;

							if (item.idtask) {
								await baseController.todoGetItemP(item.idtask, (todoItem: any) => {
									item.idinttask = todoItem.idint;
								});
							}
						}
					}
					//console.log("updateIdForTable:" + i + ", " + journalItems[i].name);
				}
				json = JSON.stringify(ret);
			} catch (error) {
				console.error(error);
			} finally {
				//ctx.response.status = 200;
			}

			ctx.response.status = 200;
			ctx.response.body = json;
			//}

			//asyncRunner();
			//});
		});

	async function journalGetItem(journalId: string, journalDate: number, cb: any) {
		var sql = `SELECT ${dbController.getSQLValue(
			"id",
			SQLTYPE.UUID,
			"t"
		)} as id, t.name as name, t.notes as notes, t.url as url, t.done as done, t.status as status, t.priority as priority, t.datedue as datedue, t.idusercreated as idusercreated, t.datecreated as datecreated, t.idusermodified as idusermodified, t.datemodified as datemodified  FROM todo t`;

		if (utils.isUUID(journalId)) {
			sql += " WHERE t.id = " + dbController.setSQLValue(journalId, SQLTYPE.UUID);
		} else {
			sql += " WHERE t.datedue = " + dbController.setSQLValue(journalDate, SQLTYPE.NUMBER);
		}

		sql += " AND t.type = " + dbController.setSQLValue(TODOTYPE.JOURNAL, SQLTYPE.NUMBER);

		var ret: any;
		var rows = await db.query(sql);

		if (rows.length > 0) {
			ret = rows[0];
		}
		if (cb) {
			cb(ret);
		}
	}
}
