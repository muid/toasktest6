import { ViewModelBase, ViewModelTagList, xhrPost } from "../../../common/base/public/base.ts";

var ko: any = {};
var document: any = {};
var Quill: any = {};

var viewModelJournalList: ViewModelJournalList;
var viewModelTagList: ViewModelTagList;

var TODOSTATUS = {
	ACTIVE: 0,
	INACTIVE: 1,
	ARCHIVED: 2,
};

class ViewModelJournalList extends ViewModelBase {
	self = this;
	itemsModified = ko.observable(false);
	itemsJournal = ko.observableArray([]);
	addTagItemName = ko.observable("");
	/*
	getJournalList = function (cb) {
		this.getBaseEntityItemList(this.userId, 1, (items) => {
			this.items(items);
			if (cb) {
				cb();
			}
		});
	};*/

	getTodoNotes(data: any) {
		//replace \n with <br>
		var ret = data;
		if (data && data.notehtml) {
			//ret = data.notehtml;
			ret = data.notesHtmlDisplay();
		} else if (data && data.notes) {
			ret = data.notes;
		}

		return ret;
	}

	getTodoNotesLine(notes: any, showAll: boolean) {
		var maxLen = 50;
		var ret = "";

		if (showAll) {
			return this.getTodoNotes(notes);
		} else {
			if (notes) {
				var sp = notes.split("\n");
				if (sp.length > 0) {
					ret = sp[0];
					if (ret.length > maxLen) {
						ret = ret.substr(0, maxLen) + "...";
					}
				}
			}
		}

		return ret;
	}
	getJournalList(type: number, cb: any) {
		var apiMethod = "/api/v1/GetJournalItemList";

		var req = {
			userId: viewModelJournalList.userId,
			type: type,
			tagBase: "",
			tagSearch: "",
			tagIds: null,
			showDone: viewModelJournalList.searchShowDone,
			showDel: viewModelJournalList.searchShowDel,
		};

		/*
        if (viewModelJournalList.searchTagItems.length > 0) {
            req.tagIds = getTagIds(viewModelJournalList.searchTagItems);
            apiMethod = "/todo/apigetJournalListByTags";
        }*/

		xhrPost(apiMethod, ko.toJSON(req), true, (json: any) => {
			for (var i = 0; i < json.length; i++) {
				json[i].showDetails = ko.observable(false);
				json[i].showEdit = ko.observable(false);
				json[i].showFullNotes = ko.observable(false);
				json[i].notesDisplay = ko.observable(this.getTodoNotesLine(json[i].notes, false));
				json[i].notesHtmlDisplay = ko.observable(this.utils.replaceCodeUrlWithAnchor(json[i].notehtml, viewModelJournalList.userName()));
				json[i].tags = getTagsForTodoItemId(json[i].id);
				json[i].datedueDisplay = this.utils.getDateStrFromMS(json[i].datedue);
				json[i].datedue = json[i].datedue;
				json[i].datePicker = null;
				json[i].isEdit = ko.observable(false);

				json[i] = ko.observable(json[i]);
			}

			viewModelJournalList.items(json);
			if (cb) {
				cb();
			}
		});
	}

	refresh(type: number, cb: any) {
		//this.refreshBase((tag2Todos) => {
		this.getJournalList(type, () => {
			if (cb) {
				cb();
			}
		});
		//});
	}

	saveJournalItemBase(data: any, event: any, onlyUpdate: boolean) {
		var currentDate = new Date();
		var today = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
		var dueDateStr = today.getTime();
		var todayStr = this.utils.getDateStrFromMS(today.getTime());

		if (onlyUpdate) {
			todayStr = data.name;
			dueDateStr = data.duedate;
			data.notes = data.inpItemCapture().getText();
			data.notehtml = data.inpItemCapture().getHtml();
		} else {
			//data = this.createJournalItemFromNote();
		}

		var req = {
			id: data.id,
			userid: this.userId,
			type: 1,
			idnote: data.idnote,
			status: data.status,
			name: todayStr,
			notes: data.notes,
			notehtml: data.notehtml,
			done: false,
			priority: 0,
			//status: 0,
			url: "",
			duedate: dueDateStr,
			onlyUpdateNote: onlyUpdate,
		};

		xhrPost("/api/v1/JournalUpd", ko.toJSON(req), false, (json: any) => {
			this.toggleEdit(data);
			viewModelJournalList.addTodoItemName("");
			var delta = viewModelJournalList.inpCapture().theme.quill.clipboard.convert("");
			viewModelJournalList.inpCapture().setContents(delta);
			//this.getJournalList(1, undefined);
			//this.getJournalList(3, undefined);
		});

		return true;
	}

	saveJournalItem(data: any, event: any) {
		return this.saveJournalItemBase(data, event, false);
	}

	showElement(elem: { style: { display: string } }, show: any) {
		//  var paragraph = elem.querySelector(".hidden");
		if (elem.style.display == "none") {
			elem.style.display = "block";
		} else {
			elem.style.display = "none";
		}
	}
	showEditor(itemId: string, show: boolean) {
		var elem = document.getElementById("bubble_" + itemId);
		if (elem) {
			this.showElement(elem.parentElement, show);
		}
	}
	toggleEdit(item: any) {
		if (item.showEdit()) {
			item.showEdit(false);
		} else {
			item.showEdit(true);
		}
	}

	toggle(item: any) {
		if (item.showDetails()) {
			item.showDetails(false);
			this.showEditor(item.id, false);

			item.isEdit(false);
		} else {
			item.showDetails(true);

			if (!(item.inpItemCapture && typeof item.inpItemCapture == "function")) {
				item.inpItemCapture = ko.observable({});
				item.inpItemCapture(
					new Quill("#bubble_" + item.idnote, {
						placeholder: "Enter a note for the new todo item...",
						theme: "snow",
					})
				);

				var delta = item.inpItemCapture().theme.quill.clipboard.convert(item.notehtml);
				item.inpItemCapture().setContents(delta);
			} else {
				this.showEditor(item.id, true);
			}
			item.isEdit(true);
		}
	}

	addToTaskItem(data: any, event: any) {
		//var self = this;
		var idint = prompt("Please enter the TASK Id");

		var req = {
			intId: idint,
			type: 4,
		};

		xhrPost("/todo/apiGetTodoItemByIdInt", ko.toJSON(req), true, (json: any) => {
			if (json) {
				var id = json.id;
				data.idtask = id;
				data.idjournal = id;
				data.idfk = id;

				//this.addUpdateNote(data, (resp: any) => {});
			}
		});

		this.fakeTest();

		return null;
	}
	fakeTest() {
		init("", "");
		initUnsorted("", "");
		this.getTodoStatusColor(TODOSTATUS.ACTIVE, false);
		this.getTodoNotes(undefined);
	}
	/*getTodoStatusColor(status, done, inActive) {
		var ret = "";
	
		if (!inActive) {
			ret = "colorItemActive";
	
			if (done) {
				ret = "colorItemDone";
			} else {
				if (status == TODOSTATUS.INACTIVE) {
					ret = "colorItemInActive";
				} else if (status == TODOSTATUS.ARCHIVED) {
					ret = "colorItemArchived";
				}
			}
		}
	
		return ret;
	}*/
	//self.items : ko.observableArray([]);
}
/*
var createviewModelJournalList = function () {
    var self = this;

    self.userId = ko.observable(-1);
    self.userName = ko.observable("");    
    self.userList = ko.observable([]);
    self.searchTagUrl = ko.observable(""); 
    self.searchTagUrlUsed = ko.observable(false);
    self.searchShowDone = ko.observable(false);
    self.searchShowDel = ko.observable(false);
    self.showInp = ko.observable(true);
    self.inpCapture = ko.observable(null);
    self.inpItemCapture = ko.observable(null);
    
    self.items = ko.observableArray([]);
    self.tagItems = ko.observableArray([]);
    self.tag2TodoItems = ko.observableArray([]);
    self.addTodoItemName = ko.observable("");
    self.searchTagItems = ko.observableArray([]);
    self.searchTerm = ko.observable("");
    self.itemsModified = ko.observable(false);
   
    self.foo = function(data) {
        //return [];
        return ko.observable([]);
    }

    self.refresh = function(data, event) {
        getJournalList();
        return true;
    };

    self.delTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDelTodoItem', ko.toJSON(req), false, (json) => {
            getJournalList();
        });
    };

    self.disableTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDisableTodoItem', ko.toJSON(req), false, (json) => {
            getJournalList();
        });
    };

    self.archiveTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiArchiveTodoItem', ko.toJSON(req), false, (json) => {
            getJournalList();
        });
    };
    

    self.removeSearchTag = function(data, event) {
        if (!(self.searchTagUrlUsed() && self.searchTagItems.length == 1)) {
            var tagIndex = getIndexById(self.searchTagItems, data.id);
            self.searchTagItems.splice(tagIndex, 1);
            getJournalList();
        }  
    };

    self.removeTagRelation = function(data, event) {
        //viewModelTag.tagId = data;
    
        xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTagList));
    };

 
    self.deleteItem = function(data, event) {
        data.status = 2;
        self.updateJournalItem(data, event);
    }
        
    self.updateJournalItem = function(data, event) {
        var noteHtml = "";
        var noteText = "";

        
        if (data.inpItemCapture && typeof data.inpItemCapture =="function") {
            noteHtml = data.inpItemCapture().getHtml();
            noteText = data.inpItemCapture().getText();
        }

        if (noteHtml && noteHtml.length > 0 ) {
            if (data.notehtml != noteHtml) {
                data.notehtml = noteHtml;
                data.notes = noteText;
            }
        } 
        
        return self.saveJournalItemBase(data, event, true);
    }



    self.toggle = function(item) {
        if (item.showDetails()) {
            item.showDetails(false);
            showEditor(item.id, false);
    
            item.isEdit(false);
        } else {
            item.showDetails(true);
            
            
            if (!(item.inpItemCapture && typeof item.inpItemCapture =="function")) {
                item.inpItemCapture = ko.observable({});
                item.inpItemCapture(new Quill('#bubble_'+item.idnote, {
                    placeholder: 'Enter a note for the new todo item...',
                    theme: 'snow' 
                }));
               
                var delta = item.inpItemCapture().theme.quill.clipboard.convert(item.notehtml);
                item.inpItemCapture().setContents(delta);
            }
            else {
                showEditor(item.id, true);
            }
            item.isEdit(true);
        }
 
    };

    self.toggleEdit = function(item) {
        if (item.showEdit()) {
            item.showEdit(false);
        } else {
            item.showEdit(true);
        }
    }

    self.toggleShowFullNotes = function(item) {
        if (item.showFullNotes()) {
            item.showFullNotes(false);
            item.notesDisplay(getTodoNotesLine(item.notes, false));
        } else {
            item.showFullNotes(true);
            item.notesDisplay(getTodoNotesLine(item.notes, true));
        }
    }

    self.addTagItemName = ko.observable("");
    
    self.delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
 };


*/

/*
setTextEditor = function() {

    var delta = item.inpItemCapture().theme.quill.clipboard.convert(item.notehtml);
    item.inpItemCapture().setContents(delta);
}*/
/*
var createViewModelTagList = function () {
	var self = this;

	self.items = ko.observableArray([]);
	self.addTagItemName = ko.observable("");

	self.delTagItem = function (data, event) {
		var req = {
			tagId: data.id,
		};

		xhrPost("/todo/apiDelTag", ko.toJSON(req), false, (json) => {
			getTagList();
		});
	};
};

var createViewModelJournalList = function () {
	var self = this;

	self.itemsJournal = ko.observableArray([]);
	self.addTagItemName = ko.observable("");
};
*/

/*
function clearInput(id: any) {
	document.getElementById(id).value = "";
}

self.createItem = function (name, note, noteHtml, showDetails, showEdit) {
	var item = {
		id: -1,
		name: name,
		notes: note,
		notehtml: noteHtml,
		done: "",
		priority: "",
		url: "",
		showDetails: ko.observable(showDetails),
		showEdit: ko.observable(showEdit),
		showFullNotes: ko.observable(false),
		notesDisplay: ko.observable(""),
		datedueDisplay: ko.observable(""),
		tags: [],
		idusercreated: -1,
		datecreated: 0,
		idusermodified: -1,
		datemodified: 0,
	};

	return item;
};

function addTodoItem() {
	getTitleFromUrl(viewModelJournalList.addTodoItemName, (htmlTitle) => {
		var title = viewModelJournalList.addTodoItemName;
		var url = "";

		if (htmlTitle && htmlTitle.length > 0) {
			title = htmlTitle;
			url = viewModelJournalList.addTodoItemName;
		}

		var item = createItem(title, "", "", true, true);
		item.url = url;
		viewModelJournalList.items().push(new ko.observable(item));
		viewModelJournalList.searchTerm("");
	});
}
function checkDeltaForFormatting(delta) {
	var ret = false;

	for (var i = 0; i < delta.ops.length; i++) {
		var currentChange = delta.ops[i];
		if (currentChange.attributes) {
			ret = true;
		}
	}

	return ret;
}

function createJournalItemFromNote() {
	var note = "";
	var noteHtml = "";

	note = viewModelJournalList.inpCapture().getText();
	noteHtml = viewModelJournalList.inpCapture().getHtml();
	
    //var delta = viewModelJournalList.inpCapture().getContents(); 
    //if (checkDeltaForFormatting(delta)) {
    //    noteHtml = viewModelJournalList.inpCapture().getHtml();
    //    //note = viewModelJournalList.inpCapture().getHtml();
    //} else {
    //    note = viewModelJournalList.inpCapture().getText();
    //}

	var item = createItem("", note, noteHtml, true, true);
	return item;
}

function setSearchTerm(data) {
	viewModelJournalList.addTodoItemName(data.name);
	viewModelJournalList.searchTerm(data.name);
	setTimeout(() => {
		data.showDetails(true);
	}, 100);
}

function addTodoItemold() {
	var req = {
		name: viewModelJournalList.addTodoItemName,
	};

	xhrPost("/todo/apiUpd", ko.toJSON(req), false, (json) => {
		viewModelJournalList.addTodoItemName("");
		//clearInput('inpAddTodoItem');
		//getJournalList();
		refresh();
	});
}*/
/*
function addTagItem() {
    var req = {
        name : viewModelJournalList.addTagItemName
    };

    xhrPost('/todo/apiAddTag', ko.toJSON(req), false, (json) => {
        
        viewModelJournalList.addTagItemName("");
        getTagList();
    });
}
*/
/*
function addTagRelation(data) {
	var req = {
		tagId: 0,
		tagName: viewModelJournalList.addTagItemName,
		todoItemId: data.id,
	};

	xhrPost("/todo/apiAddTagRelation", ko.toJSON(req), false, (json) => {
		viewModelJournalList.toggleEdit(data);
		viewModelJournalList.addTagItemName("");
		init();
		//getTagList();
	});
}*/
/*
function refresh() {
	var userId = viewModelJournalList.userId();

	//getTagList(userId, (tags) => {
	//getTag2TodoList(userId, (tag2Todos) => {
	getJournalList(userId);
	//});
	//});
}
*/
function init(userId: string, userName: string) {
	//viewModelJournalList = new createviewModelJournalList();
	//viewModelDueList = new createViewModelDueList();
	//viewModelTagList = new createViewModelTagList();
	viewModelJournalList = new ViewModelJournalList();
	ko.applyBindings(viewModelJournalList, document.getElementById("tdTodoListId"));
	//ko.applyBindings(viewModelJournalList, document.getElementById("idJournalList"));

	//ko.applyBindings(viewModelDueList, document.getElementById("tdDueListId"));
	//ko.applyBindings(viewModelTagList, document.getElementById("tdTagListId"));

	viewModelJournalList.getUserList(undefined);

	/*
    var searchTagName = '#{tags}';
    if (searchTagName) {
        var searchTag = getTagByName(searchTagName);
        viewModelJournalList.searchTagItems.push(searchTag);
    }*/

	if (userId && viewModelJournalList.userId() == -1) {
		viewModelJournalList.userId(userId);
	}

	if (userName && viewModelJournalList.userName() == "") {
		viewModelJournalList.userName(userName);
	}

	viewModelJournalList.refresh(1, undefined);

	Quill.prototype.getHtml = function () {
		return this.container.querySelector(".ql-editor").innerHTML;
	};

	viewModelJournalList.inpCapture(
		new Quill("#bubble-container", {
			placeholder: "Enter a note for the new todo item...",
			theme: "snow",
		})
	);

	/*
    var searchTagName = '#{tags}';
    if (searchTagName) {
      viewModelJournalList.searchTagUrl(searchTagName);
    }
*/
	/*
	var ac = autoComplete({
		selector: 'input[name="q"]',
		minChars: 0,
		onSelect: function (e, term, item) {
			var k = 2;
			var tag = getTagByName(term);
			viewModelJournalList.searchTagItems.push(tag);
			document.getElementById("inpAddTodoItem").value = "";
			getJournalList();
		},
		onSuggestNone: function (term) {
			viewModelJournalList.searchTerm(term);
			var t = 0;
		},
		source: function (term, suggest) {
			term = term.toLowerCase();
			var choices = getTagNames(); // ['ActionScript', 'AppleScript', 'Asp'];
			var matches = [];
			for (i = 0; i < choices.length; i++) {
				var item = choices[i].toLowerCase();

				//var index  = ~choices[i].toLowerCase().indexOf(term);
				var index = item.indexOf(term);
				//if (~choices[i].toLowerCase().indexOf(term)) {
				if (index != -1) {
					matches.push(choices[i]);
				} else {
					if (term == " ") {
						matches.push(choices[i]);
					}
				}
			}
			suggest(matches, term);
		},
	});*/
}

function initUnsorted(userId: string, userName: string) {
	//viewModelJournalList = new createviewModelJournalList();
	//viewModelDueList = new createViewModelDueList();
	//viewModelTagList = new createViewModelTagList();
	viewModelJournalList = new ViewModelJournalList();
	ko.applyBindings(viewModelJournalList, document.getElementById("tdTodoListId"));
	//ko.applyBindings(viewModelJournalList, document.getElementById("idJournalList"));

	//ko.applyBindings(viewModelDueList, document.getElementById("tdDueListId"));
	//ko.applyBindings(viewModelTagList, document.getElementById("tdTagListId"));

	viewModelJournalList.getUserList(undefined);

	/*
    var searchTagName = '#{tags}';
    if (searchTagName) {
        var searchTag = getTagByName(searchTagName);
        viewModelJournalList.searchTagItems.push(searchTag);
    }*/

	if (userId && viewModelJournalList.userId() == -1) {
		viewModelJournalList.userId(userId);
	}

	if (userName && viewModelJournalList.userName() == "") {
		viewModelJournalList.userName(userName);
	}

	viewModelJournalList.refresh(3, undefined);

	Quill.prototype.getHtml = function () {
		return this.container.querySelector(".ql-editor").innerHTML;
	};

	viewModelJournalList.inpCapture(
		new Quill("#bubble-container", {
			placeholder: "Enter a note for the new todo item...",
			theme: "snow",
		})
	);
}
/*
function getTagIndexById(id) {
    var ret = null;

    for (var i = 0; i < viewModelJournalList.tagItems.length; i++) {
        var element = viewModelJournalList.tagItems()[i];
        if (element.id == id) {
            ret = i;
             
            break;
        }
    }

    return ret;
}
function getTagById(id) {
    var ret = null;

    for (var i = 0; i < viewModelJournalList.tagItems.length; i++) {
        var element = viewModelJournalList.tagItems()[i];
        if (element.id == id) {
            ret = element;
             
            break;
        }
    }

    return ret;
}
*/
function getTagNames() {
	var ret = [];

	for (var i = 0; i < viewModelJournalList.tagItems.length; i++) {
		var element = viewModelJournalList.tagItems()[i];
		ret.push(element.name);
	}

	return ret;
}

function getTagIds(tagItems: any) {
	var ret = [];

	for (var i = 0; i < tagItems.length; i++) {
		var element = tagItems()[i];
		if (element) {
			ret.push(element.id);
		}
	}

	return ret;
}

function getTagByName(name: string) {
	var ret = null;

	for (var i = 0; i < viewModelJournalList.tagItems.length; i++) {
		var element = viewModelJournalList.tagItems()[i];
		if (element.name == name) {
			ret = element;

			break;
		}
	}

	return ret;
}

function getTagsForTodoItemId(id: string) {
	/*var ret = [];

	if (id != -1) {
		viewModelJournalList.tag2TodoItems().forEach((element) => {
			if (element.idtodo == id) {
				var tag = getById(viewModelJournalList.tagItems(), element.idtag);
				ret.push(tag);
			}
		});
	}

	return ret;*/
}
/*
function getJournalList(userId) {
	var apiMethod = "/api/v1/GetJournalItemList";

	var req = {
		userId: viewModelJournalList.userId,
		type: 1,
		tagBase: "",
		tagSearch: "",
		tagIds: null,
		showDone: viewModelJournalList.searchShowDone,
		showDel: viewModelJournalList.searchShowDel,
	};

	if (viewModelJournalList.searchTagItems.length > 0) {
		req.tagIds = getTagIds(viewModelJournalList.searchTagItems);
		apiMethod = "/todo/apigetJournalListByTags";
	}

	xhrPost(apiMethod, ko.toJSON(req), true, (json) => {
		for (var i = 0; i < json.length; i++) {
			json[i].showDetails = ko.observable(false);
			json[i].showEdit = ko.observable(false);
			json[i].showFullNotes = ko.observable(false);
			json[i].notesDisplay = ko.observable(getTodoNotesLine(json[i].notes, false));
			json[i].notesHtmlDisplay = ko.observable(utils.replaceCodeUrlWithAnchor(json[i].notehtml, viewModelJournalList.userName()));
			json[i].tags = getTagsForTodoItemId(json[i].id);
			json[i].datedueDisplay = utils.getDateStrFromMS(json[i].datedue);
			json[i].datedue = json[i].datedue;
			json[i].datePicker = null;
			json[i].isEdit = ko.observable(false);

			json[i] = ko.observable(json[i]);
		}
 

		viewModelJournalList.items(json);
	});
}
*/
/*
function getTagList(userId, cb) {
	viewModelTagList.todoItemId = -1;

	xhrPost("/todo/apiGetTagList", ko.toJSON(viewModelTagList), true, (json) => {
		viewModelJournalList.tagItems(json);
		if (viewModelJournalList.searchTagUrl()) {
			viewModelJournalList.searchTagUrlUsed(true);
			var searchTag = getTagByName(viewModelJournalList.searchTagUrl());
			viewModelJournalList.searchTagItems.push(searchTag);

			viewModelJournalList.searchTagUrl("");
		}

		if (cb) {
			cb(viewModelJournalList.tagItems);
		}
	});
}
*/
/*
function getUserList(cb: any) {
	xhrPost("/todo/apiGetUserList", null, true, (json: any) => {
		viewModelJournalList.userList(json);
		if (cb) {
			cb(viewModelJournalList.tagItems);
		}
	});
}*/
/*
function getTag2TodoList(userId:string, cb:any) {
	viewModelTagList.todoItemId = -1;

	xhrPost("/todo/apiGetTag2TodoList", ko.toJSON(viewModelTagList), true, (json) => {
		viewModelJournalList.tag2TodoItems(json);
		if (cb) {
			cb(viewModelJournalList.tag2TodoItems);
		}
	});
}

function getTitleFromUrl(url, cb) {
	var myUrl = url;
	if (typeof url == "function") {
		myUrl = url();
	}

	if (!utils.isUrl(myUrl)) {
		cb("");
		return;
	}

	var req = {
		url: url,
	};

	xhrPost("/todo/apiGetTitleFromUrl", ko.toJSON(req), true, (json) => {
		if (cb) {
			cb(json);
		}
	});
}



function getTodoStatusText(status) {
	var ret = "";

	if (status == TODOSTATUS.INACTIVE) {
		ret = "inactive";
	} else if (status == TODOSTATUS.ARCHIVED) {
		ret = "archived";
	}

	return ret;
}
*/

/*
function getTodoPriorityText(prio) {
	var ret = prio;

	if (prio <= 0) {
		ret = "";
	} else if (prio == "null") {
		ret = "";
	}

	return ret;
}



function getTodoEditUrl(id) {
	return "/todo/edit/" + id;
}
*/
/*
function getTodoSearchUrl(tag) {
    return '/todo?searchTag=' + tag;
}*/
/*
function getTodoSearchUrl(tag) {
	//var tag = item.name;
	return "/todo/" + tag;
}

function getUserName(userId) {
	var ret = "";

	var user = getById(viewModelJournalList.userList(), userId);
	if (user && user.id) {
		ret = user.name;
	}

	return ret;
}
function getCreatedModifiedStr(userIdCreated, dateCreated, userIdModified, dateModified) {
	var ret = "";
	var u = getById(viewModelJournalList.userList(), userIdCreated);
	if (userIdCreated) {
		ret = `Created by ${getUserName(userIdCreated)} at ${utils.getDateTimeStrFromMS(dateCreated)}`;

		if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
			ret += `, modified by ${getUserName(userIdModified)} at ${utils.getDateTimeStrFromMS(dateModified)}`;
		}
	}

	return ret;
}

function showDatePicker(data, ev) {
	if (!data.datePicker) {
		data.datePicker = setDatePicker(data, ev.target);
	}

	if (data.datePicker) {
		data.datePicker.show();
	}
}

function getDueDateVal(data) {
	var ret = data.datedue;

	if (data.datePicker) {
		ret = utils.getMSFromDateStr(data.datePicker.element.value);
	}

	return ret;
}

function setDatePicker(data, elem) {
	return new Datepicker(elem, {
		// ...options
		format: "dd.mm.yyyy",
		weekStart: 1,
		//showOnClick: true,
		todayBtn: true,
		todayBtnMode: 1,
		todayHighlight: true,
		//autohide: true,
		changeDate: (d) => {
			var i = 0;
		},
	});
}*/
