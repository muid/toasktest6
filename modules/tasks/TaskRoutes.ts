//import { Application, Request, Response } from "express";
//import sqlite from "sqlite3";
import { Router, Context, RouterContext } from "../../deps/oak.ts";
import { renderFile } from "../../deps/pug.ts";
import { DbController, SQLTYPE } from "../../common/db/DbController.ts";
import { UserController } from "../../common/users/UserController.ts";
import { TagController } from "../../common/tags/TagController.ts";
import { TaskController } from "./TaskController.ts";
import { BaseController } from "../../common/base/BaseController.ts";
import { MyContext } from "../../common/MyContext.ts";

var userController: UserController;
var tagController: TagController;
var dbController: DbController;
var baseController: BaseController;
var taskController: TaskController;

export function use(path: string, app: Router, db: DbController, myContext: MyContext) {
	//export function use(path: string, router: Router, userController: UserController) {
	//export function TasksRoute(app: Application, db: sqlite.Database, myContext: MyContext) {
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	dbController = db;
	baseController = new BaseController(db, myContext);
	taskController = new TaskController(db, myContext);

	return app
		.get("/:user/tasks", async (ctx: RouterContext) => {
			//if (path == "/:user/tasks") {
			//app.get(`${path}`, async (ctx: RouterContext) => {
			//app.get("/:user/tasks", async (ctx: RouterContext) => {
			/*if (!req.session.loggedIn) {
				return res.redirect(`./`);
			}*/

			var userName = ctx.params.user;

			if (userName) {
				userController.userGetByName(userName, (user: any) => {
					//var user = { id: 3, name: "" };
					if (user && user.id != -1) {
						//res.render("tasklist", { userid: user.id, username: user.name });
						ctx.response.body = renderFile("modules/tasks/views/tasklist.pug", { userid: user.id, username: user.name });
					}
				});
			}
		})
		.get("/:user/taskedit/:taskId", async (ctx: RouterContext) => {
			var taskId = ctx.params.taskId ?? "";
			var userName = ctx.params.user ?? "";
			//taskId = parseInt(taskId);
			if (!taskId || taskId == "0") {
				taskId = dbController.uuidCreateEmpty();
				//taskId = uuidCreate();
				//taskId = uuidToShortUuid(taskId);
			}

			if (taskId && userName) {
				var user = await userController.userGetByNameBase(userName);
				if (user && user.id != -1) {
					var tags = await tagController.tagGetItemsBase();
					var itemTags = await tagController.tagGetItemsForTodoItemBase(taskId); //, (itemTags: any[]) => {
					var todoItem = await baseController.todoGetItemBase(taskId); //, (todoItem: any) => {
					if (todoItem) {
						var body = "";
						try {
							body = renderFile("modules/tasks/views/taskedit.pug", { todoitem: todoItem, tags: tags, itemTags: itemTags, user: user });
							ctx.response.body = body;
						} catch (ex) {
							var k = ex;
						}
					} else {
						ctx.response.status = 200;
						ctx.response.body = "";
					}
				}
			}
		})
		.post("/api/v1/GetTaskGroupList", async (ctx: RouterContext) => {
			const body = ctx.request.body();
			const params = await body.value;
			//var params = ctx.params;
			var userId = params.userId;

			taskController.taskGroupGetItems((items: any[]) => {
				var json = JSON.stringify(items);
				ctx.response.status = 200;
				ctx.response.body = json;
			});
		});
}
