//import { getById } from "./shared/utils";
//import utils from "../../../public/shared/utils";
//import * as base from "../../../common/base/public/base";
//import { Utils } from "../../../public/shared/utils";
//import { ViewModelEditBase, ViewModelNotesBase } from "../../../common/base/public/base";

//import { TODOTYPE, TODONOTESTATUS } from "../../../public/shared/Types";

import { ViewModelBase, ViewModelEditBase, ViewModelNotesBase, ViewModelTagList, xhrPost, getTaskGroupItems, TODOTYPE, TODONOTESTATUS } from "../../../common/base/public/base.ts";

var ko: any = {};
var document: any = {};
var Quill: any = {};

var viewModelTodoItem: ViewModelTaskEdit;
var viewModelNotes: ViewModelTaskNotes;

//var utils: Utils;

class Status {
	statusName: string;
	statusId: number;

	constructor(name: string, id: number) {
		this.statusName = name;
		this.statusId = id;
	}
}

class GroupItem {
	id: string;
	idint: number;
	name: string;

	constructor(name: string, id: string, idint: number) {
		this.id = id;
		this.idint = idint;
		this.name = name;
	}
}

class ViewModelTaskEdit extends ViewModelEditBase {
	availableStatus = ko.observableArray([new Status("New", 0), new Status("WaitingForAnswer", 5), new Status("Closed", 20)]);

	init(userId: string, userName: string, cb: any) {
		this.username(userName);
		this.userid(userId);
		this.getUserList(() => {});
		getTaskGroupItems({}, (groupItems: Array<GroupItem>) => {
			//viewModelTodoItem.taskGroupItems = ko.observable(items);
			this.groupname(this.getTaskGroupNameById(this.idgroup));

			for (var i = 0; i < groupItems.length; i++) {
				var groupItem = new GroupItem(groupItems[i].name, groupItems[i].id, i + 1);
				this.taskGroupItems.push(groupItem);
				if (groupItem.id == this.idgroup()) {
					this.idintgroup(groupItem.idint);
				}
			}

			if (cb) {
				cb();
			}
		});
	}
	saveTaskItem() {
		viewModelTodoItem.idgroup(this.getGroupIdFromIntId(viewModelTodoItem.idintgroup()));

		this.saveItem(TODOTYPE.TASK, (json) => {
			location.href = `/${viewModelTodoItem.username()}/taskedit/${json}/`;
		});
	}

	getTaskGroupNameById(id: string) {
		var ret = "";

		var item = this.utils.getById(viewModelTodoItem.taskGroupItems(), id, "");
		if (item) {
			ret = item.name;
		}

		return ret;
	}

	getGroupIdFromIntId(idint: number) {
		var ret = null;

		for (var i = 0; i < viewModelTodoItem.taskGroupItems.length; i++) {
			var item = viewModelTodoItem.taskGroupItems()[i];
			if (item.idint == idint) {
				ret = item.id;
				break;
			}
		}

		return ret;
	}

	/*
function getTaskGroupNames() {
	var ret = [];
	for (var i = 0; i < viewModelTodoItem.taskGroupItems().length; i++) {
		ret.push(viewModelTodoItem.taskGroupItems()[i].name);
	}

	return ret;
}


function getTaskGroupIdByName(name) {
	var ret = "";

	for (var i = 0; i < viewModelTodoItem.taskGroupItems().length; i++) {
		var item = viewModelTodoItem.taskGroupItems()[i];
		if (item.name.toLowerCase() == name.toLowerCase()) {
			ret = item.id;
			break;
		}
	}

	return ret;
}



function getGroupIntIdFromId(id) {
	var ret = null;

	for (var i = 0; i < viewModelTodoItem.taskGroupItems.length; i++) {
		var item = viewModelTodoItem.taskGroupItems()[i];
		if (item.id == id) {
			ret = i + 1;
			break;
		}
	}

	return ret;
} */
}

class ViewModelTaskNotes extends ViewModelNotesBase {
	addTaskNote(taskItem: any) {
		var self = this;

		var req = {
			idnote: null,
			idfk: typeof taskItem.id == "function" ? taskItem.id() : taskItem.id,
			//userid: typeof taskItem.userid == "function" ? taskItem.userid() : taskItem.userid, //,
			userid: viewModelTodoItem.userid(),
			status: TODONOTESTATUS.ACTIVE,
			notes: "",
			notehtml: "",
		};

		if (taskItem.inpCapture && taskItem.inpCapture()) {
			req.notes = taskItem.inpCapture().getText();
			req.notehtml = taskItem.inpCapture().getHtml();
		}

		this.addUpdateTaskNote(req, (resp: any) => {
			var delta = taskItem.inpCapture().theme.quill.clipboard.convert("");
			taskItem.inpCapture().setContents(delta);

			self.getTaskNotes(req.idfk, (notes: any) => {
				viewModelNotes.noteItems(notes);
			});
			init("", "", "");
		});
	}
}

function init(userId: string, userName: string, taskId: string) {
	//var self = this;

	//utils = new Utils();
	viewModelNotes = new ViewModelTaskNotes(taskId);
	//viewModelNotes.getItem(taskId, (taskItem) => {
	//getTaskItem(taskId, (taskItem) => {

	viewModelTodoItem = new ViewModelTaskEdit();

	viewModelTodoItem.getItem(taskId, true, (taskItem) => {
		viewModelTodoItem.init(userId, userName, () => {
			ko.applyBindings(viewModelTodoItem, document.getElementById("myBody"));
			//ko.applyBindings(viewModelTag, document.getElementById("tagBody"));

			Quill.prototype.getHtml = function () {
				return this.container.querySelector(".ql-editor").innerHTML;
			};

			viewModelTodoItem.inpCapture(
				new Quill("#bubble-container", {
					placeholder: "Enter a new note for this task ...",
					theme: "snow",
				})
			);
		});
		//getTaskGroupItems({}, (groupItems) => {
		//viewModelTodoItem.taskGroupItems = ko.observable(items);
		/*viewModelTodoItem.groupname(viewModelTodoItem.getTaskGroupNameById(viewModelTodoItem.idgroup));

			for (var i = 0; i < groupItems.length; i++) {
				var groupItem = new GroupItem(groupItems[i].name, groupItems[i].id, i + 1);
				viewModelTodoItem.taskGroupItems.push(groupItem);
				if (groupItem.id == viewModelTodoItem.idgroup()) {
					viewModelTodoItem.idintgroup(groupItem.idint);
				}
			}*/
		/*
			var ac = autoComplete({
				selector: 'input[name="q"]',
				//selector: 'idTaskGroup',

				minChars: 0,
				onSelect: function (e, term, item) {
					viewModelTodoItem.idgroup(getTaskGroupIdByName(term));

				},
				onSuggestNone: function (term) {
					var t = 0;
				},
				source: function (term, suggest) {
					term = term.toLowerCase();
					var choices = getTaskGroupNames();
					var matches = [];
					for (i = 0; i < choices.length; i++) {
						var item = choices[i].toLowerCase();

						var index = item.indexOf(term);
						if (index != -1) {
							matches.push(choices[i]);
						} else {
							if (term == " ") {
								matches.push(choices[i]);
							}
						}
					}
					suggest(matches, term);
				},
			}); */
		//});
	});

	viewModelNotes.getTaskNotes(taskId, (notes: any) => {
		viewModelNotes.noteItems(notes);
		ko.applyBindings(viewModelNotes, document.getElementById("notesBody"));
	});
}
