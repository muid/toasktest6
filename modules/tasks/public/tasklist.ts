//import utils from "../../../public/shared/utils";
import { ViewModelBase, ViewModelTagList, xhrPost } from "../../../common/base/public/base.ts";

var ko: any = {};
var document: any = {};

//import { ViewModelTagList } from "../../../common/base/public/base";

var viewModelTaskList: ViewModelTaskList;
var viewModelTagList: ViewModelTagList;

class ViewModelTaskList extends ViewModelBase {
	getTaskItemList = (cb: any) => {
		this.getBaseEntityItemList(this.userId, 4, (items) => {
			this.items(items);
			if (cb) {
				cb();
			}
		});
	};

	refresh = (cb: any) => {
		this.getBaseEntityItemList(this.userId, this.utils.TODOTYPEDATA.TASK.ID, (items: any) => {
			this.items(items);
			if (cb) {
				cb(items);
			}
		});
		/*this.refreshBase((tag2Todos) => {
			this.getTaskItemList((tag2Todos) => {
				if (cb) {
					cb();
				}
			});
		});*/
	};

	saveTodoItem = (data: any, event: any) => {
		var req = {
			id: data.id,
			userid: this.userId,
			idnote: data.idnote,

			name: data.name,
			notes: data.notes,
			notehtml: data.notehtml,
			done: data.done,
			priority: data.priority,
			status: data.status,
			url: data.url,
			//duedate: this.utils.getDueDateVal(data),
		};

		xhrPost("/todo/apiUpd", ko.toJSON(req), false, (json: any) => {
			/*self.toggleEdit(data);
			viewModelTodoList.addTodoItemName("");
			getTodoItemList();*/
		});
		init("", "");
		return true;
	};
}

function init(userId: any, userName: any) {
	if (!userId) {
		return null;
	}
	viewModelTaskList = new ViewModelTaskList();
	viewModelTagList = new ViewModelTagList();

	if (userId && viewModelTaskList.userId() == -1) {
		viewModelTaskList.userId(userId);
	}

	if (userName && viewModelTaskList.userName() == "") {
		viewModelTaskList.userName(userName);
	}

	viewModelTaskList.getUserList(() => {
		viewModelTaskList.refresh(() => {
			ko.applyBindings(viewModelTaskList, document.getElementById("tdTodoListId"));
		});

		/*
        viewModelTaskList.getTaskItemList( (items) => {
            var hier = 1;
            //refresh();

        //ko.applyBindings(viewModelTaskList, document.getElementById("tdTodoListId"));
        } )*/
	});
}

/*
function refresh() {
    var userId = viewModelTaskList.userId();

    getTagList(userId, (tags) => {
        getTag2TodoList(userId, (tag2Todos) => {
            getTodoItemList(userId, (tag2Todos) => {
            });        
        });
    });
}*/

function clearInput(id: any) {
	document.getElementById(id).value = "";
}

/*
function getTagList(userId, cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/api/v1/GetTagList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tagItems(json);
        if (viewModelTodoList.searchTagUrl()) {
            viewModelTodoList.searchTagUrlUsed(true);
            var searchTag = getTagByName(viewModelTodoList.searchTagUrl());
            viewModelTodoList.searchTagItems.push(searchTag);
            
            viewModelTodoList.searchTagUrl("");
        }

        if (cb) {
            cb(viewModelTodoList.tagItems);
        }
    });
}



function getTodoItemList(userId) {
    var apiMethod = '/api/v1/GetTodoItemList';
    
    var req = {
        userId : viewModelTodoList.userId,
        type : 4,
        tagBase:  "",
        tagSearch:  "",
        tagIds : null,
        showDone: viewModelTodoList.searchShowDone,
        showDel: viewModelTodoList.searchShowDel
    };

    if (viewModelTodoList.searchTagItems.length > 0) {
        req.tagIds = getTagIds(viewModelTodoList.searchTagItems);
        apiMethod = '/api/v1/GetTodoItemListByTags';
    }


    xhrPost(apiMethod, ko.toJSON(req), true, (json) => {
        for (var i =0 ; i < json.length; i++) {
            json[i].showDetails = ko.observable(false);
            json[i].showEdit = ko.observable(false);
            json[i].showFullNotes = ko.observable(false);
            json[i].notesDisplay = ko.observable(getTodoNotesLine(json[i].notes, false));
            json[i].tags = getTagsForTodoItemId(json[i].id);
            json[i].datedueDisplay = utils.getDateStrFromMS(json[i].datedue);
            json[i].datedue = json[i].datedue;
            json[i].datePicker = null;
            
            json[i] = ko.observable(json[i]);
        }
      

        viewModelTodoList.items(json);
        //viewModelDueList.items(json);
    });
}*/
