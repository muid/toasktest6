//import sqlite from "sqlite3";
import { SQLTYPE, DbController } from "../../common/db/DbController.ts";
import { MyContext } from "../../common/MyContext.ts";

export class TaskController {
	//db: sqlite.Database;
	dbController: DbController;

	//implements BaseController<IUser>
	constructor(db: DbController, myContext: MyContext) {
		//this.db = db;
		this.dbController = db;
		//this.repository = new AppAuthRepository(db.client);
	}

	async taskGroupGetItems(cb: any) {
		var sql = `SELECT 	${this.dbController.getSQLValue("id", SQLTYPE.UUID, "t")} as id,`;
		sql += `${this.dbController.getSQLValue("idint", SQLTYPE.NUMBER, "t")} as idint,`;
		sql += `${this.dbController.getSQLValue("name", SQLTYPE.TEXT, "t")} as name,`;
		sql += `${this.dbController.getSQLValue("status", SQLTYPE.NUMBER, "t")}  as status,`;
		sql += `${this.dbController.getSQLValue("priority", SQLTYPE.NUMBER, "t")}  as priority,`;
		sql += `${this.dbController.getSQLValue("idusercreated", SQLTYPE.UUID, "t")} as idusercreated,`;
		sql += `${this.dbController.getSQLValue("datecreated", SQLTYPE.NUMBER, "t")} as datecreated,`;
		sql += `${this.dbController.getSQLValue("idusermodified", SQLTYPE.UUID, "t")} as idusermodified,`;
		sql += `${this.dbController.getSQLValue("datemodified", SQLTYPE.NUMBER, "t")} as datemodified`;
		sql += ` FROM taskGroup t`;

		sql += " ORDER BY name";

		//this.db.all(sql, [], (err, rows) => {
		var rows = await this.dbController.query(sql);
		//updateId("TaskGroup", 1, 'id');
		//updateIdForTable("TaskGroup", "idint", "id");

		if (cb) {
			cb(rows);
		}
	}
}
