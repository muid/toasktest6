
function convertDocxToHtml(inputDocFilePathWithFileName, cb) {
	mammoth
		.convertToHtml(
			{
				path: inputDocFilePathWithFileName,
			},
			null
		)
		.then(function (result) {
			var html = result.value; // The generated HTML
			if (cb) {
				cb(result);
			}

			var messages = result.messages; // Any messages, such as warnings during conversion
			console.log(messages);
		})
		.done();
}


function createKBFromDocx(filePath, cb?) {
	var fileName = path.basename(filePath);

	convertDocxToHtml(filePath, (res) => {
		var notesHtml = escapeText(res.value);
		var notesText = striptags(notesHtml);

		var item = {
			id: uuidCreate(),
			userid: uuidCreateEmpty(),
			idnote: uuidCreateEmpty(),
			type: TODOTYPE.KB,
			name: fileName,
			notes: notesText,
			notehtml: notesHtml,
			done: 0,
			priority: 0,
			status: TODOSTATUS.ACTIVE,
			url: "",
			duedate: 0,
		};

		todoAddOrUpdate(
			item.id,
			item.userid,
			null,
			null,
			item.type,
			item.name,
			item.notes,
			item.url,
			item.done,
			item.priority,
			item.status,
			item.duedate,
			(lastId) => {
				if (lastId) {
					noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, (lastIdNote) => {
						todoUpdateNoteId(lastId, lastIdNote, () => {
							/*if (cb) {
							cb();
						}*/
						});
					});
				}
			}
		);
	});
}