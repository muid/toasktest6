import { ViewModelBase, ViewModelTagList, xhrPost, TODOSTATUS } from "../../../common/base/public/base.ts";

var ko: any = {};
var document: any = {};
var Quill: any = {};
var autoComplete: any;
var viewModelKbList: ViewModelKbList;
var suggest: any;

class ViewModelKbList extends ViewModelBase {
	//var self = this;

	userId = ko.observable(-1);
	userName = ko.observable("");
	userList = ko.observable([]);
	searchTagUrl = ko.observable("");
	searchTagUrlUsed = ko.observable(false);
	searchShowDone = ko.observable(false);
	searchShowDel = ko.observable(false);
	showInp = ko.observable(true);
	inpCapture = ko.observable(null);
	showDetails = ko.observable();
	showEdit = ko.observable();
	items = ko.observableArray([]);
	tagItems = ko.observableArray([]);
	tags = ko.observableArray([]);
	tag2TodoItems = ko.observableArray([]);
	addTodoItemName = ko.observable("");
	addExcludeTag = ko.observable("");
	searchTagItems = ko.observableArray([]);
	searchTagExcludeItems = ko.observableArray([]);
	searchTerm = ko.observable("");
	itemsModified = ko.observable(false);
	filteredItems = ko.pureComputed(() => {
		//var ret =ko.observableArray([]);
		var ret = [];
		if (this.itemsModified()) {
			this.itemsModified(false);
		}
		/*if (self.searchTerm()) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().name.indexOf(viewModelTodoList.searchTerm()) != -1) {
                    ret.push(item);
                }
            }
        } else */
		{
			if (this.items() && this.items().length > 0) {
				//ret(self.items());
				var index = -1;

				for (var i = 0; i < this.items().length; i++) {
					var item: any = this.items()[i];
					if (typeof item == "function") {
						item = item();
					}
					if (item.id == -1) {
						index = i;
					}
				}

				if (index != -1) {
					var newItem = this.items.splice(index, 1)[0];
					this.items.unshift(newItem);
				}

				ret = this.items();
			}
		}

		return ret;
	}, self);

	getTagsForTodoItemId(id: string) {
		var ret: Array<any> = [];

		//if (id != -1)
		{
			this.tag2TodoItems().forEach((element: any) => {
				if (element.idtodo == id) {
					var tag = this.utils.getById(this.tagItems(), element.idtag, "");
					ret.push(tag);
				}
			});
		}

		return ret;
	}

	getTodoItemList() {
		var apiMethod = "/api/v1/GetTodoItemList";

		var req = {
			userId: this.userId,
			type: this.utils.TODOTYPEDATA.KB.ID,
			searchTerm: this.searchTerm,
			tagBase: "",
			tagSearch: "",
			tagIds: [undefined],
			tagIdsExclude: [undefined],
			showDone: this.searchShowDone,
			showDel: this.searchShowDel,
		};

		if (this.searchTagItems.length > 0) {
			req.tagIds = this.getTagIds(this.searchTagItems);
			apiMethod = "/api/v1/GetTodoItemListByTags";
		}

		if (this.searchTagExcludeItems.length > 0) {
			req.tagIdsExclude = this.getTagIds(this.searchTagExcludeItems);
			apiMethod = "/api/v1/GetTodoItemListByTags";
		}

		xhrPost(apiMethod, ko.toJSON(req), true, (json: Array<any>) => {
			for (var i = 0; i < json.length; i++) {
				json[i].showDetails = ko.observable(false);
				json[i].showEdit = ko.observable(false);
				json[i].showFullNotes = ko.observable(false);
				json[i].notesDisplay = ko.observable(this.getTodoNotesLine(json[i].notes, false));
				json[i].tags = this.getTagsForTodoItemId(json[i].id);
				json[i].datedueDisplay = this.utils.getDateStrFromMS(json[i].datedue);
				json[i].datedue = json[i].datedue;
				json[i].datePicker = null;

				json[i] = ko.observable(json[i]);
			}

			this.items(json);
			//viewModelDueList.items(json);
		});
	}

	refresh(data: any, event: any) {
		this.getTodoItemList();
		return true;
	}
	/*
    delTodoItem(data: any, event: any) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDelTodoItem', ko.toJSON(req), false, (json: Object) => {
            getTodoItemList();
        });
    };

    disableTodoItem(data: any, event: any) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDisableTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };

    archiveTodoItem(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiArchiveTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };*/

	removeSearchTag(data: any, event: any) {
		if (!(this.searchTagUrlUsed() && this.searchTagItems.length == 1)) {
			var tagIndex = this.utils.getIndexById(this.searchTagItems, data.id, "");
			this.searchTagItems.splice(tagIndex, 1);
			this.getTodoItemList();
		}
	}

	removeSearchTagExclude(data: any, event: any) {
		if (!(this.searchTagUrlUsed() && this.searchTagExcludeItems.length == 1)) {
			var tagIndex = this.utils.getIndexById(this.searchTagExcludeItems, data.id, "");
			this.searchTagExcludeItems.splice(tagIndex, 1);
			this.getTodoItemList();
		}
	}

	searchForTerm(data: any, event: any) {
		this.getTodoItemList();
	}
	/*
    removeTagRelation(data: any, event: any) {
        //viewModelTag.tagId = data;
    
        xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTagList), false, undefined);
    };*/
	saveTodoItem(data: any, event: any) {
		var req = {
			id: data.id,
			userid: this.userId,
			idnote: data.idnote,
			type: 2,
			name: data.name,
			notes: data.notes,
			notehtml: data.notehtml,
			done: data.done,
			priority: data.priority,
			status: data.status,
			url: data.url,
			//duedate: this.getDueDateVal(data)
		};

		xhrPost("/todo/apiUpd", ko.toJSON(req), false, (json: Object) => {
			this.toggleEdit(data);
			//viewModelTodoList.addTodoItemName("");
			this.getTodoItemList();
		});

		return true;
	}

	toggle(item: any) {
		if (item.showDetails()) {
			item.showDetails(false);
		} else {
			item.showDetails(true);
			//acForId(item);
		}
		/* if (self.showInp()) {
            self.showInp(false);
        } else {
            self.showInp(true);
        }*/
	}

	toggleEdit(item: any) {
		if (item.showEdit()) {
			item.showEdit(false);
		} else {
			item.showEdit(true);
		}
	}

	toggleShowFullNotes(item: any) {
		if (item.showFullNotes()) {
			item.showFullNotes(false);
			item.notesDisplay(this.getTodoNotesLine(item.notes, false));
		} else {
			item.showFullNotes(true);
			item.notesDisplay(this.getTodoNotesLine(item.notes, true));
		}
	}

	addTagItemName = ko.observable("");

	delTagItem(data: any, event: any) {
		var req = {
			tagId: data.id,
		};

		xhrPost("/todo/apiDelTag", ko.toJSON(req), false, (json: Object) => {
			//this.getTagList();
		});
	}
	fakeTest() {
		init("", "");
		this.getTodoStatusColor(TODOSTATUS.ACTIVE, false);
	}

	autoCompleteForId(sel: string, onSelect: any, onSuggestNone: any) {
		//var sel = "#auto" + item.id;

		return autoComplete({
			selector: sel,
			minChars: 0,
			onSelect: function (e: any, term: string, element: any) {
				if (onSelect) {
					onSelect(term);
				}
			},
			onSuggestNone: function (searchTerm: string) {
				if (onSuggestNone) {
					onSuggestNone(searchTerm);
				}
			},
			source: function (term: string, suggest: any) {
				term = term.toLowerCase();
				var choices = this.getTagNames();
				var matches = [];
				for (var i = 0; i < choices.length; i++) {
					var item = choices[i].toLowerCase();

					var index = item.indexOf(term);
					if (index != -1) {
						matches.push(choices[i]);
					} else {
						if (term == " ") {
							matches.push(choices[i]);
						}
					}
				}
				suggest(matches, term);
			},
		});
	}

	acForId(item: any) {
		var self = this;
		var itemId = item.id;
		var sel = "#auto" + item.id;

		item.ac = this.autoCompleteForId(
			sel,
			(term: string) => {
				var tag = this.getTagByName(term);
				var myItem = this.utils.getById(viewModelKbList.items(), itemId, "");
				myItem.selectedTag = tag;
			},
			undefined
		);
	}

	addTodoItemNote() {
		var note = "";
		var noteHtml = "";
		var delta = this.inpCapture().getContents();
		note = this.inpCapture().getText();
		if (this.checkDeltaForFormatting(delta)) {
			noteHtml = this.inpCapture().getHtml();
			//note = viewModelTodoList.inpCapture().getHtml();
		} else {
			note = this.inpCapture().getText();
		}

		var item = this.createItem("", note, noteHtml, true, true);
		this.items().push(new ko.observable(item));
		this.itemsModified(true);
		//viewModelTodoList.searchTerm("");
	}

	/*
refresh() {

}*/
	init() {
		var sel = "#inpAddTodoItem";
		var a = this.autoCompleteForId(
			sel,
			(term: any) => {
				var tag = this.getTagByName(term);
				this.searchTagItems.push(tag);
				document.getElementById("inpAddTodoItem").value = "";
				this.getTodoItemList();
			},
			(searchTerm: string | any[]) => {
				if (searchTerm && searchTerm.length > 0) {
					console.log(searchTerm + ":" + searchTerm.length);
					if (searchTerm[searchTerm.length - 1] == " ") {
						this.searchTerm(searchTerm);
						this.getTodoItemList();
					}
				}
			}
		);

		var sel = "#inpSearchNot";
		this.autoCompleteForId(
			sel,
			(term: string) => {
				var tag = this.getTagByName(term);
				this.searchTagExcludeItems.push(tag);
				document.getElementById("inpSearchNot").value = "";
				this.getTodoItemList();
			},
			undefined
		);
	}

	addTagRelationEdit(data: any) {
		this.addTagRelation(data, () => {
			var elementId = "auto" + data.id;
			document.getElementById(elementId).value = "";
			//this.refresh();
			data.showDetails(false);
		});
	}

	addTagRelation(data: any, cb: any) {
		var req = {
			todoItemId: data.id,
			tagId: "",
			tagName: "",
		};

		if (data.selectedTag) {
			req.tagId = data.selectedTag.id;
		} else {
			req.tagName = this.addTagItemName;
		}

		xhrPost("/todo/apiAddTagRelation", ko.toJSON(req), false, (json: any) => {
			if (cb) {
				cb();
			}
		});
	}

	getTodoSearchUrl(tag: any) {
		//var tag = item.name;
		return "/todo/" + tag;
	}
}
/*
var createViewModelTagList = function () {
    var self = this;

    items = ko.observableArray([]);
    addTagItemName = ko.observable("");
    
    delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
}

function clearInput(id) {
    document.getElementById(id).value = ""; 
}

self.createItem = function(name, note, noteHtml, showDetails, showEdit) {
    var item = {
        id : -1,
        idint : 0,
        name: name,
        notes: note,
        notehtml: noteHtml,
        done : "",
        priority: "",
        url: "",
        showDetails: ko.observable(showDetails),
        showEdit: ko.observable(showEdit),
        showFullNotes : ko.observable(false),
        notesDisplay: ko.observable(""),
        datedueDisplay: ko.observable(""),
        tags: [],
        idusercreated: -1,
        datecreated : 0,
        idusermodified : -1,
        datemodified: 0
    }

    return item;
}

function addTodoItem() {
    

    getTitleFromUrl(viewModelTodoList.addTodoItemName, (htmlTitle) => {
        var title = viewModelTodoList.addTodoItemName; 
        var url = "";

        if (htmlTitle && htmlTitle.length > 0) {
            title = htmlTitle;
            url = viewModelTodoList.addTodoItemName; 
        }

        var item = createItem(title, "", "", true, true);
        item.url = url;
        viewModelTodoList.items().push(new ko.observable(item));
        viewModelTodoList.searchTerm("");
        document.getElementById("auto-1").value = "";
    });
    
    
    
}

function setSearchTerm(data) {
    viewModelTodoList.addTodoItemName(data.name);
    viewModelTodoList.searchTerm(data.name);
    setTimeout(() => {
        data.showDetails(true);
    }, 100);
    
}

function addTodoItemold() {
    var req = {
        name : viewModelTodoList.addTodoItemName
    };

    xhrPost('/todo/apiUpd', ko.toJSON(req), false, (json) => {
        viewModelTodoList.addTodoItemName("");
        //clearInput('inpAddTodoItem');
        //getTodoItemList();
        refresh();
    });
}
 

function addTagRelationSearch(data) {
    addTagRelation(data, () => {
        viewModelTodoList.toggleEdit(data);
        viewModelTodoList.addTagItemName("");

        init();
    });
}


function refresh() {
    var userId = viewModelTodoList.userId();

    getTagList(userId, (tags) => {
        getTag2TodoList(userId, (tag2Todos) => {
            getTodoItemList(userId, (tag2Todos) => {
            });        
        });
    });
}
*/
function init(userId: string, userName: string) {
	viewModelKbList = new ViewModelKbList();
	//viewModelTodoList = new createViewModelTodoList();
	//viewModelDueList = new createViewModelDueList();
	//viewModelTagList = new createViewModelTagList();
	ko.applyBindings(viewModelKbList, document.getElementById("tdTodoListId"));
	//ko.applyBindings(viewModelDueList, document.getElementById("tdDueListId"));

	//ko.applyBindings(viewModelTagList, document.getElementById("tdTagListId"));

	viewModelKbList.getUserList(undefined);

	if (userId && viewModelKbList.userId() == -1) {
		viewModelKbList.userId(userId);
	}

	if (userName) {
		viewModelKbList.userName(userName);
	}

	viewModelKbList.refresh(undefined, undefined);

	Quill.prototype.getHtml = function () {
		return this.container.querySelector(".ql-editor").innerHTML;
	};
	viewModelKbList.inpCapture(
		new Quill("#bubble-container", {
			placeholder: "Enter a some knowledge ...",
			theme: "snow",
		})
	);
	viewModelKbList.init();
}

/*







function getTagList(userId, cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/todo/apiGetTagList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tagItems(json);
        if (viewModelTodoList.searchTagUrl()) {
            viewModelTodoList.searchTagUrlUsed(true);
            var searchTag = getTagByName(viewModelTodoList.searchTagUrl());
            viewModelTodoList.searchTagItems.push(searchTag);
            
            viewModelTodoList.searchTagUrl("");
        }

        if (cb) {
            cb(viewModelTodoList.tagItems);
        }
    });
}

function getUserList(cb) {
    xhrPost('/todo/apiGetUserList', null, true, (json) => {
        viewModelTodoList.userList(json);
        if (cb) {
            cb(viewModelTodoList.tagItems);
        }
    });
}



function getTag2TodoList(userId, cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/todo/apiGetTag2TodoList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tag2TodoItems(json);
        if (cb) {
            cb(viewModelTodoList.tag2TodoItems);
        }
    });
}

function getTitleFromUrl(url, cb) {
    var myUrl = url;
    if (typeof url =='function') {
        myUrl = url();
    }

    if (!utils.isUrl(myUrl)) {
        cb("");
        return;
    }

    var req = {
        url: url
    };

    xhrPost('/todo/apiGetTitleFromUrl', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    
}

var TODOSTATUS = {
	ACTIVE : 0,
	INACTIVE : 1,
	ARCHIVED : 2
};

function getTodoStatusText(status) {
    var ret = "";
    
    if (status == TODOSTATUS.INACTIVE) {
        ret = "inactive";
    } else if (status == TODOSTATUS.ARCHIVED) {
        ret = "archived";
    }

    return ret;
}

function getTodoStatusColor(status, done) {
    var ret = "colorItemActive";

    if (done) {
        ret = "colorItemDone";
    } else {
        if (status == TODOSTATUS.INACTIVE) {
            ret = "colorItemInActive";
        } else if (status == TODOSTATUS.ARCHIVED) {
            ret = "colorItemArchived";
        }
    }

    return ret;
}

function getTodoPriorityText(prio) {
    var ret = prio;
    
    if (prio<= 0) {
        ret = "";
    } else if (prio  == "null") {
        ret = "";
    }

    return ret;
}




function getTodoEditUrl(id) {
    return '/todo/edit/' + id;
}


 


function getUserName(userId) {
    var ret = "";

    var user = getById(viewModelTodoList.userList(), userId);
    if (user && user.id) {
        ret = user.name;
    }

    return ret;
}
function getCreatedModifiedStr(userIdCreated, dateCreated, userIdModified, dateModified) {
    var ret = ""; 
    var u = getById(viewModelTodoList.userList(), userIdCreated);
    if (userIdCreated) {
        ret = `Created by ${getUserName(userIdCreated)} at ${utils.getDateTimeStrFromMS(dateCreated)}`;
        
        if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
            ret += `, modified by ${getUserName(userIdModified)} at ${utils.getDateTimeStrFromMS(dateModified)}`;
        }
    }

    return ret;
}

function showDatePicker(data, ev) {
    if (!data.datePicker) {
        data.datePicker = setDatePicker(data, ev.target);
        
    } 
    
    if (data.datePicker) {
        data.datePicker.show();
    }
}

function getDueDateVal(data) {
    var ret = data.datedue;

    if (data.datePicker) {
        ret = utils.getMSFromDateStr(data.datePicker.element.value);
    } 
    
    return ret;
}

function setDatePicker(data, elem) {
    return new Datepicker(elem, {
        // ...options
        format: "dd.mm.yyyy",
        weekStart: 1,
        //showOnClick: true,
        todayBtn: true,
        todayBtnMode: 1,
        todayHighlight: true,
        //autohide: true,
        changeDate: (d) => {
            var i = 0;
        }
    });
}*/
