/*
import { Application, Request, Response } from "express";
import sqlite from "sqlite3";
import { DbController, SQLTYPE} from "../../common/controllers/DbController"
import { UserController  } from "../../common/controllers/UserController"
import { TagController  } from "../../common/controllers/TagController"
import { BaseModule } from "../base/BaseModule"
*/
import { Router, Context, RouterContext } from "../../deps/oak.ts";
import { renderFile } from "../../deps/pug.ts";
import { DbController, SQLTYPE } from "../../common/db/DbController.ts";
import { UserController } from "../../common/users/UserController.ts";
import { TagController } from "../../common/tags/TagController.ts";
import { KbController } from "./KbLogic.ts";
import { BaseController } from "../../common/base/BaseController.ts";
import { MyContext } from "../../common/MyContext.ts";
//import {} from "../../common/"
import { ViewModelBase, ViewModelEditBase, ViewModelNotesBase, ViewModelTagList, xhrPost, getTaskGroupItems, TODOTYPE, TODOSTATUS, TODONOTESTATUS } from "../../common/base/public/base.ts";
import { Utils } from "../../public/src/shared/Utils.ts";

var userController: UserController;
var tagController: TagController;
var dbController: DbController;
var baseController: BaseController;
var utils: Utils;

export function use(path: string, app: Router, db: DbController, myContext: MyContext) {
	userController = new UserController(db, myContext);
	tagController = new TagController(db, myContext);
	dbController = db;
	baseController = new BaseController(db, myContext);
	utils = new Utils();

	return app
		.get("/:user/kb", (ctx: RouterContext) => {
			/*if (!req.session.loggedIn) {
			return res.redirect(`./`);
		}*/
			var f = "d:/dev/nodejs/test1/input.docx";

			//createKBFromDocx(f);

			var userName = ctx.params.user;

			if (userName) {
				userController.userGetByName(userName, (user: any) => {
					if (user && user.id != -1) {
						//updateIdIntForTable("todo", 2);
						//res.render("kblist", { userid: user.id, username: user.name });
						ctx.response.body = renderFile("modules/kb/views/kblist.pug", { userid: user.id, username: user.name });
					}
				});
			}
		})
		.get("/:user/kbedit/:kbId", (ctx: RouterContext) => {
			var kbId: string = "";
			if (ctx.params.kbId) {
				kbId = ctx.params.kbId;
			}
			//taskId = parseInt(taskId);

			if (kbId) {
				var userName = ctx.params.user;

				if (userName) {
					userController.userGetByName(userName, (user: any) => {
						if (user && user.id != -1) {
							tagController.tagGetItems((tags: any) => {
								tagController.tagGetItemsForTodoItem(kbId, (itemTags: any) => {
									baseController.todoGetItem(kbId, (todoItem: any) => {
										todoItem.kbId = utils.createIdStrForIdInt(todoItem.idint, 2);
										ctx.response.body = renderFile("modules/journal/views/kbedit.pug", {
											todoitem: todoItem,
											tags: tags,
											itemTags: itemTags,
											user: user,
										});
									});
								});
							});
						} else {
						}
					});
				}
			}
			//getList(0, 0, res);
		});
}
