import { SQLTYPE, DbController } from "../../common/db/DbController.ts";
import { MyContext } from "../../common/MyContext.ts";

export class KbController {
	dbController: DbController;
	constructor(db: DbController, myContext: MyContext) {
		//this.db = db;
		this.dbController = db;
		//this.repository = new AppAuthRepository(db.client);
	}

	getSQLKBSync(type: number, idParent: string) {
		var sql = `SELECT ${this.dbController.getSQLValueShort("id", SQLTYPE.UUID, "t")},`;
		sql += `t.md5,`;
		sql += `t.name,`;
		sql += `t.tagname,`;
		sql += `t.path,`;
		sql += `t.error,`;
		sql += `t.datesynced`;
		sql += ` FROM KBSync t`;

		sql += ` WHERE t.type = ${type}`;

		if (idParent) {
			sql += ` AND idparent = ${this.dbController.setSQLValue(idParent, SQLTYPE.UUID)}`;
		}

		return sql;
	}

	async kbGetSyncList(type: number, idParent: string, cb: any) {
		var sql = this.getSQLKBSync(type, idParent);

		var res = await this.dbController.query(sql); //, [], (err, rows) => {
		if (cb) {
			cb(res.rows);
		}
	}

	async kbSyncAddOrUpdate(id: number, parentId: string, kbId: string, type: number, md5: string, name: string, tagName: string, path: string, error: string, datesynced?: number, cb?: any) {
		var sql = `INSERT INTO KbSync(id, idparent, idkb, type, md5, name, tagname, path, error, datesynced) VALUES(${this.dbController.setSQLValue(id, SQLTYPE.UUID)},${this.dbController.setSQLValue(parentId, SQLTYPE.UUID)},${this.dbController.setSQLValue(
			kbId,
			SQLTYPE.UUID
		)},${this.dbController.setSQLValue(type, SQLTYPE.NUMBER)},"${this.dbController.setSQLValue(md5, SQLTYPE.TEXT)}","${this.dbController.setSQLValue(name, SQLTYPE.TEXT)}","${this.dbController.setSQLValue(
			tagName,
			SQLTYPE.TEXT
		)}","${this.dbController.setSQLValue(path, SQLTYPE.TEXT)}","${this.dbController.setSQLValue(error, SQLTYPE.TEXT)}","${this.dbController.setSQLValue(datesynced, SQLTYPE.NUMBER)}")`;

		if (id > 0) {
			sql = `UPDATE Tag SET name = '${this.dbController.setSQLValue(name, SQLTYPE.TEXT)}',`;
			sql += `idparent = '${this.dbController.setSQLValue(parentId, SQLTYPE.UUID)}',`;
			sql += `idkb = '${this.dbController.setSQLValue(kbId, SQLTYPE.UUID)}',`;
			sql += `type = '${this.dbController.setSQLValue(type, SQLTYPE.NUMBER)}',`;
			sql += `md5 = '${this.dbController.setSQLValue(md5, SQLTYPE.TEXT)}',`;
			sql += `name = '${this.dbController.setSQLValue(name, SQLTYPE.TEXT)}'`;
			sql += `tagname = '${this.dbController.setSQLValue(tagName, SQLTYPE.TEXT)}'`;
			sql += `path = '${this.dbController.setSQLValue(path, SQLTYPE.TEXT)}'`;
			sql += `error = '${this.dbController.setSQLValue(error, SQLTYPE.TEXT)}'`;
			sql += `datesynced = '${this.dbController.setSQLValue(datesynced, SQLTYPE.TEXT)}'`;
			sql += `WHERE id = ${this.dbController.setSQLValue(id, SQLTYPE.UUID)}`;
		}

		var res = await this.dbController.query(sql);
		if (cb) {
			cb();
		}
	}
	/*
  kbSyncItems() {
	//kbSyncAddOrUpdate(null, null, 1, "", "", "", "", "", 0);

	kbGetSyncList(1, null, (dirItems: Array<any>) => {
		for (var i = 0; i < dirItems.length; i++) {
			var dir = dirItems[i];

			kbGetSyncList(2, dir.idparent, (fileItems : Array<any>) => {
				var cwd = dir.path;
				//cwd = 'D:/tmp/wlan/'
				var files = fs.readdirSync(cwd);
				if (files) {
					for (var j = 0; j < files.length; j++) {
						var file = files[j];
						var filePath = path.join(cwd, file);
						var buf = fs.readFileSync(filePath);
						var hash = md5(buf);
						if (hash) {
							var fileItem = utils.getById(fileItems, hash, "md5");
							if (!fileItem) {
								fileItem = utils.getById(fileItems, filePath, "path");
								if (fileItem) {
									//update item
									createKBFromDocx(filePath, (kbItem: any) => {
										kbSyncAddOrUpdate(
											fileItem.id,
											fileItem.idparent,
											0,
											2,
											hash,
											fileItem.name,
											fileItem.tagname,
											fileItem.path,
											"",
											0
										);
										var t = kbItem;
										if (t) t++;
									});
								} else {
									//new item
									createKBFromDocx(filePath, (kbItem) => {
										kbSyncAddOrUpdate(null, dir.id, 2, hash, file, file, filePath, "", 0);
									});
								}
							}
							var c = 0;
						}
					}
				}
			});
		}
	});
	
}*/
}
