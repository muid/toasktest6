export class Utils {
	TODOTYPEDATA = {
		TODO: {
			ID: 0,
			PREFIX: "TODO",
		},
		JOURNAL: {
			ID: 1,
			PREFIX: "JRNL",
		},
		KB: {
			ID: 2,
			IDLEN: 6,
			PREFIX: "KB",
		},
		TASK: {
			ID: 4,
			IDLEN: 6,
			PREFIX: "TASK",
		},
	};
	// Helper function
	toLC(str: string) {
		return str.trim().toLowerCase();
	}

	isUrl(url: string) {
		var ret = false;

		if (url) {
			url = url.toLowerCase();
			if (url.indexOf("https:") != -1 || url.indexOf("http:") != -1) {
				ret = true;
			}
		}

		return ret;
	}

	getMSFromDateStr(dateStr: any) {
		var ret = -1;

		var day = NaN;
		var month = NaN;
		var year = NaN;

		var myDateStr = dateStr;
		var i = myDateStr.indexOf(".");
		if (i != -1) {
			day = parseInt(myDateStr.substr(0, i));
		}

		myDateStr = myDateStr.substr(i + 1);
		i = myDateStr.indexOf(".");
		if (i != -1) {
			month = parseInt(myDateStr.substr(0, i));
		}

		year = parseInt(myDateStr.substr(i + 1));

		if (!(isNaN(day) && isNaN(month) && isNaN(year))) {
			var date = new Date(year, month - 1, day);
			ret = date.getTime();
		}

		return ret;
	}

	getDateStrFromMS(ms: string | number | Date) {
		var ret = "";

		if (ms) {
			var d: any = new Date(ms);
			var day = d.getDate().toString();
			if (day.length < 2) {
				day = "0" + day;
			}

			var month = (d.getMonth() + 1).toString();
			if (month.length < 2) {
				month = "0" + month;
			}

			var year = d.getYear() + 1900;
			//var ret = `${d.getDate()}.${d.getMonth()+1}.${d.getYear()+1900}`;
			var ret = `${day}.${month}.${year}`;
		}

		return ret;
	}

	getDateTimeStrFromMS(ms: string | number | Date) {
		var ret = "";

		if (ms) {
			var d = new Date(ms);

			var hours = d.getHours().toString();
			if (hours.length < 2) {
				hours = "0" + hours;
			}

			var minutes = d.getMinutes().toString();
			if (minutes.length < 2) {
				minutes = "0" + minutes;
			}

			var ret = `${this.getDateStrFromMS(ms)} ${hours}:${minutes}`;
		}

		return ret;
	}

	isUUID(id: string) {
		return (id + "").length == 32;
	}

	isUUIDEmpty(id: string) {
		var ret = id == "0".repeat(32);
		return ret;
	}

	getIndexById(arr: any, id: any, memberName: string) {
		var ret = -1;

		var myId = id;
		if (typeof id == "function") {
			myId = id();
		}

		var myArr: any = arr;
		if (typeof arr == "function") {
			myArr = arr(); //is this a knockout observable?
		}

		for (var i = 0; i < myArr.length; i++) {
			var element = myArr[i];
			if (typeof element == "function") {
				element = element();
			}

			if (!memberName) {
				memberName = "id";
			}

			if (element[memberName] == myId) {
				//if (element.id == id) {
				ret = i;

				break;
			}
		}

		return ret;
	}

	getById(arr: any[], id: string, memberName: string) {
		var ret = null;

		var index = this.getIndexById(arr, id, memberName);
		if (index != -1) {
			ret = arr[index];
			if (typeof ret == "function") {
				ret = ret();
			}
		}

		return ret;
	}

	getUrlForType(id: string, userName: string, type: number) {
		var ret = "/" + userName;

		if (type == 2) {
			ret += "/kbedit/";
		}

		if (type == 4) {
			ret += "/taskedit/";
		}

		ret += "" + id;

		return ret;
	}

	createIdStrForIdInt(idInt: string | number, type: number) {
		var ret = "";
		var idStr = idInt + "";
		var part1Len = 0;

		if (idInt != -1) {
			if (type == 0) {
				ret = "TODO";
				part1Len = 4;
			} else if (type == 2) {
				ret = "KB";
				part1Len = 2;
			} else if (type == 4) {
				ret = "TASK";
				part1Len = 4;
			}

			var filler = 6 - idStr.length;

			for (var i = 0; i < filler; i++) {
				ret += "0";
			}

			ret += idStr;
		}

		return ret;
	}

	getAnchorFromCodedUrl(codeUrl: string, userName: any) {
		var ret = codeUrl;

		if (codeUrl) {
			var parts = codeUrl.split(":");
			if (parts.length == 3) {
				var idInt = parts[1];
				var id = parts[2].substr(0, parts[2].length - 1);
				var type = parts[0].substr(2);
				var url = this.getUrlForType(id, userName, parseInt(type));
				var title = this.createIdStrForIdInt(idInt, parseInt(type));
				ret = `<a href="${url}" >${title} </a>`;
			}
		}
		//ret = `§{${type}:${idInt}:${id}}`;

		return ret;
	}

	replaceCodeUrlWithAnchor(noteHTML: string, userName: any) {
		var ret = noteHTML;
		var res = "";

		var parts = noteHTML.split(/(§\{.*?\})/g);
		if (parts && parts.length > 1) {
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				if (part[0] == "§") {
					var res2 = this.getAnchorFromCodedUrl(part, userName);
					if (res2) {
						res += res2;
					}
				} else {
					res += part;
				}
			}
			ret = res;
		}

		return ret;
	}

	cloneObject(obj: any) {
		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			var date = new Date();
			date.setTime(obj.getTime());
			return date;
		}

		// Handle Array
		if (obj instanceof Array) {
			var copyArray: Array<any> = [];
			for (var i = 0, len = obj.length; i < len; i++) {
				copyArray[i] = this.cloneObject(obj[i]);
			}
			return copyArray;
		}

		// Handle Object
		if (obj instanceof Object) {
			var copy: any = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = this.cloneObject(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	}
}
/*
    exports.TODOTYPEDATA = TODOTYPEDATA;

    exports.isUrl = isUrl;
    exports.isUUID = isUUID;
    exports.isUUIDEmpty = isUUIDEmpty;
    exports.getIndexById = getIndexById;
    exports.getById = getById;

    exports.getMSFromDateStr = getMSFromDateStr;
    exports.getDateStrFromMS = getDateStrFromMS;
    exports.getDateTimeStrFromMS = getDateTimeStrFromMS;

    exports.getUrlForType = getUrlForType;
    exports.createIdStrForIdInt = createIdStrForIdInt;
    exports.getAnchorFromCodedUrl = getAnchorFromCodedUrl;
    exports.replaceCodeUrlWithAnchor = replaceCodeUrlWithAnchor;
    */
