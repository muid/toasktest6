import { existsSync, resolve } from "./deps/denostd.ts";
import { Application, Router } from "./deps/oak.ts";
import { DbController } from "./common/db/DbController.ts";
import * as indexRouter from "./common/routes/indexRouter.ts";
import * as userRouter from "./common/routes/userRouter.ts";
import * as taskRouter from "./modules/tasks/TaskRoutes.ts";
import * as baseRouter from "./common/base/BaseRoutes.ts";
import * as journalRouter from "./modules/journal/JournalRoutes.ts";
import * as kbRouter from "./modules/kb/KbRoutes.ts";
import * as todoRouter from "./modules/todo/TodoRoutes.ts";

var port = 8000;
/*if (__dirname.indexOf("beta") != -1) {
	port = 8080;
}*/

const app = new Application();

const router = new Router();

//import express from "express";
//import sqlite from "sqlite3";
//import path from "path";

import { Config } from "./config.ts";
//import utils from "./public/shared/utils";

//import fileUpload from "express-fileupload";
//import cors from "cors";
//import bodyParser from "body-parser";

//import fs from "fs";
//import { O_DIRECTORY } from "constants";

//var mammoth = require("mammoth");
//var striptags = require("striptags");
//var md5 = require("md5");
import { MyContext } from "./common/MyContext.ts";
import { UserController } from "./common/users/UserController.ts";
/*
const app = express();

var port = 8000;
if (__dirname.indexOf("beta") != -1) {
	port = 8080;
}
*/
var db: DbController; // = null;
var myContext: MyContext = new MyContext();
var config: Config = new Config();
var userController: UserController;

//app.set("views", "./src/views");
//app.set("view engine", "pug");

//app.use(express.json());
/*
app.use(cors());
app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true,
	})
);

app.use(
	fileUpload({
		createParentPath: true,
	})
);
*/

/*
fs.stat(dbPath, (error, stats) => {
	if (error) {
		fs.copyFileSync("./db/todo.db", dbPath);
		//console.log(error);
	}
});
*/

app.addEventListener("listen", () => {
	console.log(`Listening on localhost:${port}`);
	console.log(`Server started at port ${port}`);
	//db = new DatabaseController(config.dbPath2);

	console.log("Connected to the SQlite database.");

	/*JournalRoute(app, db, myContext);
	TasksRoute(app, db, myContext);
	TodoRoute(app, db, myContext);
	BaseRoutes(app, db, myContext);
	TagRoutes(app, db, myContext);*/
});
db = new DbController(config.dbPath2, myContext);
userController = new UserController(db, myContext);
userController.userGetAll();
indexRouter.use("/", router, userController);
userRouter.use("/:user", router, userController);

taskRouter.use("/:user/tasks", router, db, myContext);
taskRouter.use("/:user/taskedit", router, db, myContext);
baseRouter.use(router, db, myContext);
journalRouter.use("/:user/journal", router, db, myContext);
journalRouter.use("/:user/unsorted", router, db, myContext);
kbRouter.use("/:user/kb", router, db, myContext);
todoRouter.use("/:user/todo", router, db, myContext);
//taskRouter.use("/:user/taskedit", router, db, myContext);

/*
router.get('/js/app.js', (ctx: RouterContext) => { //yep, route can has defferents of real file location
	ctx.response.type = "application/javascript"
	ctx.response.body = fileJS
})
*/

app.use(router.routes());
//var t = JSON.stringify(router.routes());
var t = router.routes();
app.use(router.allowedMethods());

app.use(async (context) => {
	const pathPublic = "/public";
	const root = `${Deno.cwd()}/public`;
	if (context.request.url.pathname.startsWith(pathPublic)) {
		//if (context.request.url.pathname.length > pathPublic.length + 1) {
		var fileName = context.request.url.pathname.substr(pathPublic.length + 1, context.request.url.pathname.length);
		var filePath = resolve(root, "dist", fileName);
		if (!existsSync(filePath)) {
			console.log("file not found: " + filePath);
		} else {
			context.response.body = await Deno.readFile(filePath);
		}
	}
});

await app.listen({ port });

/*
//app.use(express.static(__dirname + '/public' ));
app.use("/public/lib", express.static(path.join(__dirname, "../src/public/lib")));
app.use("/public/shared", express.static(path.join(__dirname, "../build/public/shared")));

app.use((ctx) => {});
app.get("/", (req, res, next) => {
	userController.userGetNames((userNames) => {
		res.render("index", { userNames: userNames });
	});
});

app.use("/public/muidLogo.png", express.static(path.join(__dirname, "../src/public/muidLogo.png")));
app.use("/public/muidRemove.png", express.static(path.join(__dirname, "../src/public/muidRemove.png")));

import { BaseRoutes } from "./common/base/BaseRoutes";
var w = path.join(__dirname, "../build/common/base/public");
app.use("/public/base", express.static(path.join(__dirname, "../src/common/base/public")));
app.use("/public/base/base.js", express.static(path.join(__dirname, "../build/common/base/public/base.js")));
app.use("/public/base/base.js.map", express.static(path.join(__dirname, "../build/common/base/public/base.js.map")));
app.use("/src/common/base/public/base.ts", express.static(path.join(__dirname, "../src/common/base/public/base.ts")));

import { JournalRoute } from "./modules/journal/JournalRoutes";
app.use("/public/journal", express.static(path.join(__dirname, "../src/modules/journal/public")));
app.use("/public/journal/journallist.js", express.static(path.join(__dirname, "../build/modules/journal/public/journallist.js")));
app.use("/public/journal/journallist.js.map", express.static(path.join(__dirname, "../build/modules/journal/public/journallist.js.map")));
app.use("/src/modules/journal/public/journallist.ts", express.static(path.join(__dirname, "../src/modules/journal/public/journallist.ts")));

import { TasksRoute } from "./modules/tasks/TaskRoutes";
app.use("/public/tasks", express.static(path.join(__dirname, "../src/modules/tasks/public")));
app.use("/public/tasks/taskedit.js", express.static(path.join(__dirname, "../build/modules/tasks/public/taskedit.js")));
app.use("/public/tasks/taskedit.js.map", express.static(path.join(__dirname, "../build/modules/tasks/public/taskedit.js.map")));
app.use("/src/modules/tasks/public/taskedit.ts", express.static(path.join(__dirname, "../src/modules/tasks/public/taskedit.ts")));

app.use("/public/tasks/tasklist.js", express.static(path.join(__dirname, "../build/modules/tasks/public/tasklist.js")));
app.use("/public/tasks/tasklist.js.map", express.static(path.join(__dirname, "../build/modules/tasks/public/tasklist.js.map")));
app.use("/src/modules/tasks/public/tasklist.ts", express.static(path.join(__dirname, "../src/modules/tasks/public/tasklist.ts")));

import { TodoRoute } from "./modules/todo/TodoRoutes";
app.use("/public/todo", express.static(path.join(__dirname, "../src/modules/todo/public")));
app.use("/public/todo/todoedit.js", express.static(path.join(__dirname, "../build/modules/todo/public/todoedit.js")));
app.use("/public/todo/todoedit.js.map", express.static(path.join(__dirname, "../build/modules/todo/public/todoedit.js.map")));
app.use("/public/todo/todolist.js", express.static(path.join(__dirname, "../build/modules/todo/public/todolist.js")));
app.use("/public/todo/todolist.js.map", express.static(path.join(__dirname, "../build/modules/todo/public/todolist.js.map")));
app.use("/src/modules/todo/public/todolist.ts", express.static(path.join(__dirname, "../src/modules/todo/public/todolist.ts")));

import { TagRoutes } from "./common/tags/TagRoutes";
*/
